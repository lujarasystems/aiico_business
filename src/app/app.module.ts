import { BrowserModule } from "@angular/platform-browser";
import { NgModule, NO_ERRORS_SCHEMA, Injector } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgZorroAntdModule, NZ_I18N, en_US } from "ng-zorro-antd";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { registerLocaleData } from "@angular/common";
import en from "@angular/common/locales/en";
import { SlickCarouselModule } from "ngx-slick-carousel";
import { NzSelectModule } from "ng-zorro-antd/select";
import { SharedModule } from "./shared/shared.module";
import { NzSpinModule } from 'ng-zorro-antd/spin';

// services
import { WindowRef } from "./shared/window.service";
import { GetPositionDirective } from "./pages/home/home.directive";


// components
import { NavComponent } from "./components/nav/nav.component";
import { FooterComponent } from "./components/footer/footer.component";
import { PlanCardsComponent } from "./components/plan-cards/plan-cards.component";


// pages
import { NotFoundComponent } from "./pages/not-found/not-found.component";
import { MainLayoutComponent } from "./layouts/main-layout/main-layout.component";
import { ServiceLocator } from './models/general/service-locator';
import { ProductsService } from "./services/products.service";
import { UtilityService } from './services/utility.service';
import { AutoConfirmComponent } from './components/auto-confirm/auto-confirm.component';
import { PaymentService } from './services/payment.service';
import { AllProductsComponent } from './all-products/all-products.component';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    GetPositionDirective,
    FooterComponent,
    PlanCardsComponent,
    NavComponent,
    NotFoundComponent,
    MainLayoutComponent,
    AutoConfirmComponent,
    AllProductsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgZorroAntdModule,
    NzSelectModule,
    NzSpinModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    BrowserAnimationsModule,
    SlickCarouselModule,
    HttpClientModule,
    HttpClientJsonpModule,
    SharedModule.forRoot()
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [ProductsService, UtilityService, PaymentService, { provide: NZ_I18N, useValue: en_US }, WindowRef],
  bootstrap: [AppComponent]
})


export class AppModule {
  constructor(private injector: Injector) {
    ServiceLocator.injector = this.injector;
  }
}
