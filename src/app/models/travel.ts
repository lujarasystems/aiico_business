export class Travel {
  countryId: string;
  excess: number;
  isActive: boolean;
  medical: any;
  name: string;
  noKaddr: string;
  noKrelationship: string;
  nokName: string;
  passportNumber: string;
  physicalAddrs: any;
  preMedical: boolean;
  premium: number;
  sumAssured: number;
  titleId: string;
  title: any;
  transDate: string;
  wef: string;
  wet: string;
}

