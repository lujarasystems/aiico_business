export class PersonalInfo {
  // general
  firstName: string;
  lastName: string;
  pryEmail: string;
  gender: any;
  smsTel: string;
  dateOfBirth: string;
  address: string;
}
