export class Product {
  public description: string;
  public id: string;
  public imageUrl: string;
  public name: string;
  public prefix: string;
  public reqUW: boolean;
  public renewable: boolean;
  public wef: string;
  public wet: string;
  public productCategory: string;
  public isThirdParty: boolean;
  public isActive: boolean;

  constructor() {
    this.id = "";
    this.prefix = "";
    this.name = "";
    this.reqUW = false;
    this.renewable = false;
    this.wef = "";
    this.wet = "";
    this.imageUrl = null;
    this.description = "";
    this.productCategory = "";
    this.isThirdParty = true;
    this.isActive = true;
  }
}
