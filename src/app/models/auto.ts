export class Auto {
  //motor
  bodyType: string;
  branchId: string;
  chasisNo: string;
  clientId: string;
  color: string;
  cubicCap: string;
  dlIssueDate: string;
  dlNo: string;
  engineNo: string;
  make: string;
  model: string;
  premiumAmount: number;
  productId: string;
  regNo: string;
  subclassSectCovtypeId: string;
  vehicleAmount: number;
  wefDt: string;
  wetDt: string;
  yrManft: string;
}

