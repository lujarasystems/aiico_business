export class InterswitchOptionsModel {
  public product_id: any;
  public pay_item_id: any;
  public amount: number;
  public txn_ref: string;
  public currency: any;
  public macKey: any;
  public hash: any;
  public redirect_url: string;
  public url: string;
  public name: string;
  public pay_item_name?: string;


  constructor() {
    this.product_id = '6204';
    this.pay_item_id = 103;
    this.amount = 0;
    this.txn_ref = '';
    this.currency = 566;
    this.hash = '';
    this.redirect_url = 'http://localhost:4200/result/interswitch';
    this.url = 'https://sandbox.interswitchng.com/webpay/pay';
    this.name = '';
    this.pay_item_name = '';
    this.macKey = 'E187B1191265B18338B5DEBAF9F38FEC37B170FF582D4666DAB1F098304D5EE7F3BE15540461FE92F1D40332FDBBA34579034EE2AC78B1A1B8D9A321974025C4';
  }
}
