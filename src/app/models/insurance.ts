export class InsuranceVm {

  //motor
  bodyType: string;
  branchId: string;
  chasisNo: string;
  clientId: string;
  color: string;
  cubicCap: string;
  dateOfBirth: Date;
  dlIssueDate: string;
  dlNo: string;
  engineNo: string;
  firstName: string;
  lastName: string;
  make: string;
  model: string;
  premiumAmount: any;
  productId: string;
  pryEmail: string;
  regNo: string;
  smsTel: string;
  subclassSectCovtypeId: string;
  vehicleAmount: any;
  wefDt: string;
  wetDt: string;
  yrManft: string;

  // travel
  country: string;
  countryId: string;
  excess: number;
  isActive: boolean;
  medical?: any;
  name: string;
  noKaddr: string;
  noKrelationship: string;
  nokName: string;
  passportNumber: string;
  physicalAddrs?: any;
  preMedical: boolean;
  premium: number;
  sumAssured: number;
  titleId: string;
  title: any;
  transDate: string;
  wef: Date;
  wet: Date;
  travelPurpose: string;

  //personal accident
  beneficiary: string;
  relationship: string;
  isFullYear: boolean;
  unit: number;
  genderId: string;

  // home
  preOwnership: string;
  tenancy: string;
  description: string;
  address: string;
  postalAddress: string;
  transactionRef: string;
  lgaId: string;
  valueOfContents?: any;
  townId: string;
  intermediaryId: string;
  clientCategoryId: string;
  clientCategory: string;
  homeItems: {
    name: string,
    amount: number
  }[];

  coverDuration: string;


  // shop
  clientCategories: any;
  declearation: any;
  domiciledCountry: any;
  flutterWavePubKey: string;
  flutterWaveSecKey: string;
  gender: any;
  hash: any;
  interswitchMacKey: string;
  interswitchPaymentUrl: string;
  isWemaBank: boolean;
  keepsStockBook: boolean;
  keepsStockBookInSafePlace: boolean;

  lgas: any;
  natureOfBusiness: string;
  natureOfStock: string;
  natureOfStructure: string;
  otherNatureOfStock: any;
  otherNatureOfStructure: any;
  otherStockTakingMode: any;
  otherTypeOfShop: any;
  partnerReference: any;
  ravePublicKey: string;
  referralCode: string;
  stockAmount: string;
  stockTakingMode: string;
  titles: any;
  typeOfShop: string;

  //office

  officeItems: any;
  userId: any;
  wemaBankMacKey: string;
  wemaBankPaymentUrl: string;
  yearsOfDrvExperience: any;

  //health
  occupation: string;
  accidentRate: any;

  //upload
  IdentificationUrl: string;
  UtilityBillUrl: string;
  VehicleLicenseUrl: string;
  PassportUrl: string;
  currencySymbol: any;
}

