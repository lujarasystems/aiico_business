import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonalAccidentService {

  constructor(private baseService: BaseService) { }

  GetAccidentRates(): Observable<any> {

    const url = `${this.baseService.baseUrl}/PersonalAccidentProductService/GetPersonalAccidentRates`

    return this.baseService.http
      .get<any>(url, { headers: this.baseService.headers })
      .pipe(
        catchError(this.baseService._serverError)
      );
  }


  PostPersonalAccidentSchedule(insurance): Observable<any> {

    const url = `${this.baseService.baseUrl}/PersonalAccidentProductService/PostPersonalAccidentSchedule `

    return this.baseService.http
      .post<any>(url, insurance, { headers: this.baseService.headers })
      .pipe(
        catchError(this.baseService._serverError)
      );
  }
}
