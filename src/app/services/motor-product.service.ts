import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MotorProductService {

  constructor(private baseService: BaseService) { }

  GetPostMotorSchedule(): Observable<any> {

    const url = `${this.baseService.baseUrl}/MotorProductSchedule/PostMotorSchedule`

    return this.baseService.http
      .get<any>(url, { headers: this.baseService.headers })
      .pipe(
        catchError(this.baseService._serverError)
      );
  }

  GetVehicleDetails(plateNo: string): Observable<any> {

    const url = `${this.baseService.baseUrl}/MotorProductService/GetVehicleDetails?numberPlate=${plateNo}`

    return this.baseService.http
      .get<any>(url, { headers: this.baseService.headers })
      .pipe(
        catchError(this.baseService._serverError)
      );
  }

  PostMotorSchedule(insurance): Observable<any> {

    const url = `${this.baseService.baseUrl}/MotorProductService/PostMotorSchedule`

    return this.baseService.http
      .post<any>(url, insurance, { headers: this.baseService.headers })
      .pipe(
        catchError(this.baseService._serverError)
      );
  }
}
