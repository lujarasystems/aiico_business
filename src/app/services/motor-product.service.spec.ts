import { TestBed } from '@angular/core/testing';

import { MotorProductService } from './motor-product.service';

describe('MotorProductService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MotorProductService = TestBed.get(MotorProductService);
    expect(service).toBeTruthy();
  });
});
