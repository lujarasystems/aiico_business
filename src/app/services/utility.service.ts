import { Injectable } from "@angular/core";

@Injectable()
export class UtilityService {

  constructor() {
  }

  checkIfValueExistsInArr(arr, value, property) {
    return arr.some(el => el[property] === value);
  };

  isArrayEmpty(arr: Array<any>) {
    return !arr || arr.length === 0;
  }

  isObjectEmpty(obj: Object) {
    // eslint-disable-next-line no-unused-vars
    for (const prop in obj) {
      if (Object.prototype.hasOwnProperty.call(obj, prop)) {
        return false;
      }
    }

    return true;
  };

  sortObject(obj: Object) {
    const result = {};
    Object.keys(obj)
      .sort()
      .forEach(key => result[key] = obj[key]);

    return result;
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

}
