import { Injectable } from '@angular/core';
import { catchError } from "rxjs/operators";
import { BaseService } from './base.service';
// import { BehaviorSubject } from 'rxjs';

@Injectable()
export class PaymentService {
  constructor(public baseService: BaseService) {
  }

  getTransactionDetails(txnref, amount) {
    const url = `https://portal-staging.aiicoplc.com/api/services/app/InterswitchPaymentService/GetPaymentValidationResult?transactionRef=${txnref}&amount=${amount}`;

    return this.baseService.http
      .get<any>(url, { headers: this.baseService.headers })
      .pipe(
        catchError(this.baseService._serverError)
      );
  }
}
