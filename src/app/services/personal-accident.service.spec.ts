import { TestBed } from '@angular/core/testing';

import { PersonalAccidentService } from './personal-accident.service';

describe('PersonalAccidentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PersonalAccidentService = TestBed.get(PersonalAccidentService);
    expect(service).toBeTruthy();
  });
});
