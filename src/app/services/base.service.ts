import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { throwError as observableThrowError, of } from 'rxjs';
import { ServiceLocator } from "src/app/models/general/service-locator";

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  public _headers: HttpHeaders;
  public http: HttpClient;
  public baseUrl = "https://portal-staging.aiicoplc.com/api/services/app"
  constructor() {
    this.http = ServiceLocator.injector.get(HttpClient);
  }

  public _serverError(err: any) {
    return of(null);
  }

  get headers(): HttpHeaders {
    this._headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'X-ApiKey': '1Q2WERTS345-=YU()HJLOU567$VBJSGHakusguskw88YR8AVVUSG8SDGA7',
      'Access-Control-Allow-Headers': 'Content-Type',
    });

    return this._headers;
  }

  set headers(newHeader: HttpHeaders) {
    this._headers = newHeader;
  }
}
