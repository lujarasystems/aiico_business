import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RavepayComponent } from './ravepay.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    RavepayComponent
  ],
  exports: [
    RavepayComponent
  ]
})

export class RavepaymentModule { }
