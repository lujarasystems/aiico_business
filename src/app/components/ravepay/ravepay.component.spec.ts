import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RavepayComponent } from './ravepay.component';

describe('RavepayComponent', () => {
  let component: RavepayComponent;
  let fixture: ComponentFixture<RavepayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RavepayComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RavepayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
