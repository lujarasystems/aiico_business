import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaveResultComponent } from './rave-result.component';

describe('RaveResultComponent', () => {
  let component: RaveResultComponent;
  let fixture: ComponentFixture<RaveResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaveResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaveResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
