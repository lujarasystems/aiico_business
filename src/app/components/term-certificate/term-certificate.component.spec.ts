import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermCertificateComponent } from './term-certificate.component';

describe('TermCertificateComponent', () => {
  let component: TermCertificateComponent;
  let fixture: ComponentFixture<TermCertificateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermCertificateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermCertificateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
