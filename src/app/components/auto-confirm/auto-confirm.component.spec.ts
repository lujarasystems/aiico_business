import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutoConfirmComponent } from './auto-confirm.component';

describe('AutoConfirmComponent', () => {
  let component: AutoConfirmComponent;
  let fixture: ComponentFixture<AutoConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutoConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutoConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
