import { Component, OnInit, Input } from '@angular/core';
import { InsuranceVm } from 'src/app/models/insurance';

@Component({
  selector: 'app-auto-confirm',
  templateUrl: './auto-confirm.component.html',
  styleUrls: ['./auto-confirm.component.scss']
})
export class AutoConfirmComponent implements OnInit {

  @Input('insurance') insurance: InsuranceVm;
  constructor() { }

  ngOnInit() {
  }

}
