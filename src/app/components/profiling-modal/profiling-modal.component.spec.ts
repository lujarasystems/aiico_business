import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilingModalComponent } from './profiling-modal.component';

describe('ProfilingModalComponent', () => {
  let component: ProfilingModalComponent;
  let fixture: ComponentFixture<ProfilingModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilingModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilingModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
