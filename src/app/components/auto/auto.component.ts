import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UtilService } from 'src/app/services/util.service';
import { MotorProductService } from 'src/app/services/motor-product.service';
import { InsuranceVm } from 'src/app/models/insurance';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';


@Component({
  selector: 'app-auto',
  templateUrl: './auto.component.html',
  styleUrls: ['./auto.component.scss']
})
export class AutoComponent implements OnInit {

  @Input('insurance') insurance: InsuranceVm;
  @Input() formErrors: any;

  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  bodyTypes: any[];
  yearsOfManu: any[];
  colorList: any[];
  motorSchedules: any[];
  year: any;
  make: any;
  vehicleMakeList: any[];
  vehicleModelList: any[];
  model: any;
  loadingVehicleDetails: boolean;
  loadingModel$ = new BehaviorSubject(false);
  loadingMake$ = new BehaviorSubject(false);
  selectedPlan: any;
  today = new Date();

  constructor(private utilService: UtilService, private motorService: MotorProductService) { }

  ngOnInit() {
    this.GetBodyTypes();
    this.GetManufactureYears();
    this.GetColorList();
    this.GetMotorModel(this.insurance.make, this.insurance.yrManft);

    this.selectedPlan = JSON.parse(localStorage.getItem('selectedPlan'));
    if (this.insurance && this.insurance.bodyType && this.selectedPlan.subClassCoverTypes.productName == 'Private Motor Third Party') {
      this.computeThirdPartyPremium();
    }

    if (this.insurance && this.selectedPlan.subClassCoverTypes.productName == 'Private Motor Comprehensive' && this.insurance.vehicleAmount) {
      this.computeComprehensivePremium();
    }

  }

  isInputValid(name) {
    if (this.formErrors.length) {
      return this.formErrors[0].toLowerCase().includes(name);
    }
    return false;
  };

  GetBodyTypes() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetBodyTypes().subscribe(data => {
      if (data) {
        this.bodyTypes = [...data.result];
        localStorage.setItem("bodyTypes", JSON.stringify(this.bodyTypes));
      }

      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    return differenceInCalendarDays(current, this.today) < 0;
  };

  GetManufactureYears() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetManufactureYear().subscribe(data => {
      if (data) {
        this.GetMotorMake(this.insurance.yrManft);
        this.yearsOfManu = [...data.result];
        localStorage.setItem("yearsOfManu", JSON.stringify(this.yearsOfManu));
      }

      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  GetColorList() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetColorList().subscribe(data => {
      if (data) {
        this.colorList = [...data.result];
        localStorage.setItem("colorList", JSON.stringify(this.colorList));
      }

      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  GetMotorModel(make, year) {
    this.error$.next(false);

    this.utilService.GetVehicleMakeModel(make, year).subscribe(data => {
      if (data) {
        this.vehicleModelList = [...data.result];
        localStorage.setItem("vehicleModelList", JSON.stringify(this.vehicleModelList));
      }
      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  GetMotorMake(year) {
    this.error$.next(false);
    // this.insurance.make = null;
    this.utilService.GetVehicleMake(year).subscribe(data => {
      if (data) {
        this.vehicleMakeList = [...data.result];
        localStorage.setItem("vehicleMakeList", JSON.stringify(this.vehicleMakeList));
      }
      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  selectYear(event) {
    this.GetMotorMake(event);
  }

  selectMake(event) {
    this.GetMotorModel(event, this.insurance.yrManft);
  }

  getVehicleDetails() {
    this.loadingVehicleDetails = true;
    this.loading$.next(true);
    this.error$.next(false);

    this.motorService.GetVehicleDetails(this.insurance.regNo).subscribe(res => {
      if (res) {
        this.insurance.chasisNo = res.result.licenseInfo.chasisNo;
        this.insurance.color = res.result.licenseInfo.color;
        this.insurance.engineNo = res.result.licenseInfo.engineNo;
        this.insurance.make = res.result.licenseInfo.vehicleMakeName;
        this.insurance.yrManft = res.result.licenseInfo.year;
        this.insurance.wefDt = res.result.licenseInfo.isssueDate
        this.insurance.regNo = res.result.licenseInfo.registrationNo;
        this.insurance.model = res.result.licenseInfo.model;
        this.loadingVehicleDetails = false;

        this.GetMotorMake(this.insurance.yrManft);
        this.GetMotorModel(this.insurance.make, this.insurance.yrManft);
      }

      else {
        this.loadingVehicleDetails = false;
        this.loading$.next(false);
      }

    }), error => {
      this.loadingVehicleDetails = false;
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  computeComprehensivePremium() {
    if (this.selectedPlan.subClassCoverTypes.productName == 'Private Motor Comprehensive') {
      this.insurance.vehicleAmount = this.formatAmountToNumber(this.insurance.vehicleAmount);
      this.insurance.premiumAmount = this.insurance.vehicleAmount * (this.selectedPlan.subClassCoverTypes.rate / 100);
      this.formatAmountToString(this.insurance.vehicleAmount);
    }
  }

  computeThirdPartyPremium() {
    if (this.selectedPlan.subClassCoverTypes.productName == 'Private Motor Third Party') {
      this.utilService.ComputeThirdPartyPremium(this.insurance.bodyType).subscribe(res => {
        this.insurance.premiumAmount = res.result;
        localStorage.setItem('insurance', JSON.stringify(this.insurance));
      }), error => {

      };
    }

  }

  formatAmountToString(val) {
    try {
      if (val) {
        this.insurance.vehicleAmount = this.formatAmountToNumber(val);
        this.insurance.vehicleAmount = (parseInt(val.toString().replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US');
      }
    }
    catch (e) { }
  }

  formatAmountToNumber(val) {
    if (val)
      return val = typeof val == 'number' ? val : parseFloat(val.replace(/,/g, ''));
  }

  // ComputeThirdPartyPremium(event) {
  //   this.utilService.ComputeThirdPartyPremium(event).subscribe(res => {

  //     res

  //   }), error => {
  //     // this.loadingVehicleDetails = false;

  //   }
  // }

  setBodyType() {

  }
}
