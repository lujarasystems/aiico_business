import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-error-handler',
  templateUrl: './error-handler.component.html',
  styleUrls: ['./error-handler.component.scss']
})
export class ErrorHandlerComponent implements OnInit {
  @Input() showError: boolean;
  @Input() errorStatus: any;
  @Input() errorMessage: string;
  @Output() closedModal = new EventEmitter<any>();
  @Input() showCloseModal: boolean;


  constructor() { }

  ngOnInit() {
  }

  closeModal() {
    this.closedModal.emit();
  }

}
