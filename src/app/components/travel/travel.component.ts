import { Component, OnInit, Input } from '@angular/core';
import { InsuranceVm } from 'src/app/models/insurance';
@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.scss']
})
export class TravelComponent implements OnInit {

  @Input('insurance') insurance: InsuranceVm;
  @Input() formErrors: any;

  constructor() { }

  ngOnInit() {
  }

  isInputValid(name) {
    if (this.formErrors.length) {
      return this.formErrors[0].toLowerCase().includes(name);
    }
    return false;
  };

}
