import { Component, OnInit, Input } from '@angular/core';
import { InterswitchOptionsModel } from 'src/app/models/interswitchOptions';
// import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-interswitch',
  templateUrl: './interswitch.component.html',
  styleUrls: ['./interswitch.component.scss']
})
export class InterswitchComponent implements OnInit {

  @Input() interswitchOptions: InterswitchOptionsModel;
  @Input() buttonText: string;
  @Input() showButton: boolean;
  @Input() ispolicyrenewal: boolean;

  constructor() { }

  ngOnInit() {
  }

  saveTransactionRef() {
    const transactionDetails = {
      amount: this.interswitchOptions.amount,
      product: this.interswitchOptions.pay_item_name,
      txnref: this.interswitchOptions.txn_ref,
      ispolicyrenewal: this.ispolicyrenewal
    }

    localStorage.setItem('transactionDetails', JSON.stringify(transactionDetails));
  }

}
