import { Component, OnInit, Input } from '@angular/core';
import { UtilService } from 'src/app/services/util.service';
import { BehaviorSubject } from 'rxjs';
import { InsuranceVm } from 'src/app/models/insurance';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Input('insurance') insurance: InsuranceVm;
  @Input() formErrors: any;


  localGovtAreas = [];
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  selectedPlan: any;
  plan: string;
  today = new Date();
  homeForm: FormGroup;
  items: FormArray;

  constructor(public utilService: UtilService, private _formBuilder: FormBuilder) {
    this.homeForm = this._formBuilder.group({
      name: '',
      amount: '',
      items: this._formBuilder.array([this.createItem()])
    });
  }

  ngOnInit() {
    this.getLGs();
    this.selectedPlan = JSON.parse(localStorage.getItem('selectedPlan'));
    if (this.selectedPlan.subClassCoverTypes.subClassName == 'Home Insurance') {
      this.plan = "House";

    }
    else if (this.selectedPlan.subClassCoverTypes.subClassName == 'Office Insurance') {
      this.plan = "Office"
    }
    else {
      this.plan = "Shop";
      if(this.insurance)
      this.formatAmountToString(this.insurance.stockAmount, 'stock');
    }
  }

  createItem(): FormGroup {
    return this._formBuilder.group({
      name: '',
      amount: '',
    });
  }

  isInputValid(name) {
    if (this.formErrors.length) {
      return this.formErrors[0].toLowerCase().includes(name);
    }
    return false;
  };

  getLGs() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.getLGs().subscribe(data => {
      if (data) {
        this.localGovtAreas = [...data.result];
        localStorage.setItem('lgas', JSON.stringify(this.localGovtAreas));
      }
      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  addNewItem() {
    this.items = this.homeForm.get('items') as FormArray;
    this.items.value.forEach(i => {
      i.amount = this.formatAmountToString(i.amount, null);
    });

    if (this.selectedPlan.subClassCoverTypes.productName == "Home Content Insurance") {
      this.insurance.homeItems = this.items.value;
    }
    else if (this.selectedPlan.subClassCoverTypes.productName == "Office Content Insurance") {
      this.insurance.officeItems = this.items.value;
    }
    localStorage.setItem('insurance', JSON.stringify(this.insurance));
  }

  formatAmountToString(val, stock) {
    try {

      if (val) {
        if (stock) {
          val = this.formatAmountToNumber(val);
          this.insurance.stockAmount = (parseInt(val.toString().replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US');
        }
        else {
          val = this.formatAmountToNumber(val);
          return (parseInt(val.toString().replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US');
        }

      }
    }
    catch (e) { }
  }

  formatAmountToNumber(val) {
    if (val)
      return val = typeof val == 'number' ? val : parseFloat(val.replace(/,/g, ''));
  }

  addNewField() {
    this.items = this.homeForm.get('items') as FormArray;
    this.items.push(this.createItem());

  }

  get formData() { return <FormArray>this.homeForm.get('items'); }

  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    return differenceInCalendarDays(current, this.today) < 0;
  };

  onChange(result: Date): void {
    // console.log("onChange: ", result);
  }

}
