import { Component, OnInit, Input } from '@angular/core';
import { PersonalAccidentService } from 'src/app/services/personal-accident.service';
import { BehaviorSubject } from 'rxjs';
import { InsuranceVm } from 'src/app/models/insurance';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';

@Component({
  selector: 'app-health',
  templateUrl: './health.component.html',
  styleUrls: ['./health.component.scss']
})
export class HealthComponent implements OnInit {

  @Input('insurance') insurance: InsuranceVm;
  @Input() formErrors: any;
  today = new Date();

  date = null;
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  accidentRates: any[];

  constructor(public accidentService: PersonalAccidentService) { }

  ngOnInit() {
    this.GetAccidentRates();
    if(this.insurance)
    this.insurance.premiumAmount = this.formatAmountToString(this.insurance.premiumAmount);

  }

  isInputValid(name) {
    if (this.formErrors.length) {
      return this.formErrors[0].toLowerCase().includes(name);
    }
    return false;
  };


  onChange(result: Date): void {
    // console.log("onChange: ", result);
  }

  GetAccidentRates() {
    this.loading$.next(true);
    this.error$.next(false);

    this.accidentService.GetAccidentRates().subscribe(data => {

      this.accidentRates = [...data.result];
      localStorage.setItem('accidentRates', JSON.stringify(this.accidentRates));

      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }


  formatAmountToString(val) {
    try {

      if (val) {
        val = this.formatAmountToNumber(val);
        this.insurance.premiumAmount = (parseInt(val.toString().replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US');
      }
    }
    catch (e) { }
  }

  formatAmountToNumber(val) {
    if (val)
      return val = typeof val == 'number' ? val : parseFloat(val.replace(/,/g, ''));
  }

  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    return differenceInCalendarDays(current, this.today) < 0;
  };
}
