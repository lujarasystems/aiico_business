import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { NotFoundComponent } from "./pages/not-found/not-found.component";
import { MainLayoutComponent } from "./layouts/main-layout/main-layout.component";
import { AllProductsComponent } from "./all-products/all-products.component";

const routes: Routes = [
  {
    path: "",
    component: MainLayoutComponent,
    children: [
      {
        path: "",
        loadChildren: "./pages/home/home.module#HomeModule"
      },
      {
        path: "plan/:category/:productId",
        loadChildren: "./pages/plan/plan.module#PlanModule"
      },
      {
        path: "shop/:category/:subProductId",
        loadChildren: "./pages/shop/shop.module#ShopModule"
      },
      {
        path: "register/:category/:subProductId",
        loadChildren: "./pages/register/register.module#RegisterModule"
      },
      {
        path: "payment/:category",
        loadChildren: "./pages/payment/payment.module#PaymentModule"
      },
      {
        path: "policy-renewal",
        loadChildren: "./pages/policy-renewal/policy-renewal.module#PolicyRenewalModule"
      },
      {
        path: "personal-accident",
        loadChildren: "./pages/personal-accident/personal-accident.module#PersonalAccidentModule"
      },
      {
        path: "client-profile",
        loadChildren: "./pages/client-profile/client-profile.module#ClientProfileModule"
      },
      {
        path: "agent-profile",
        loadChildren: "./pages/agent-profile/agent-profile.module#AgentProfileModule"
      },
      {
        path: "result/:paymentChannel",
        loadChildren: "./pages/result/result.module#ResultModule"
      }

    ]
  },
  {
    path: "404",
    component: NotFoundComponent
  },
  {
    path: "products",
    component: AllProductsComponent
  },
  { path: "**", redirectTo: "/404" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
