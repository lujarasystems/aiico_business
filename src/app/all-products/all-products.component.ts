
import {
  Component, OnInit, ViewChild
} from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Product } from "src/app/models/product";
import { ProfilingModalComponent } from "src/app/components/profiling-modal/profiling-modal.component";
import { ProductsService } from "src/app/services/products.service";
import { PLANICONS, HOMESLIDERCONFIG, HOMESLIDES } from "src/app/shared/data";
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.scss']
})
export class AllProductsComponent implements OnInit {

  
  @ViewChild(ProfilingModalComponent, { static: false })
  private modalComponent: ProfilingModalComponent;

  public productsByCategories$: BehaviorSubject<Object>;
  public categories$: BehaviorSubject<Array<string>>;

  slideConfig = HOMESLIDERCONFIG;
  planIcons = PLANICONS;
  slides = HOMESLIDES;
  isValidateVisible = false;

  constructor(public productService: ProductsService, public utilityService: UtilityService) {
    this.productsByCategories$ = new BehaviorSubject({});
    this.categories$ = new BehaviorSubject([]);
  }

  ngOnInit() {
    this.productService.productsByCategories$.subscribe(data => {
      if (!this.utilityService.isObjectEmpty(data)) {
        this.productsByCategories$.next(data);
        this.categories$.next(Object.keys(data));
      }
    });
  }

  handleCancel() {
    this.modalComponent.handleCancel();
  }

  showModal(value) {
    this.modalComponent.showModal(value);
  }

  scrollToDiv(el: HTMLElement) {
    el.scrollIntoView();
  }

  handleValidateCancel(): void {
    this.isValidateVisible = false;
  }

  showValidateModal(): void {
    this.isValidateVisible = true;
  }
}
