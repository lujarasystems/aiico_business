import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-main-layout",
  templateUrl: "./main-layout.component.html",
  styleUrls: ["./main-layout.component.scss"]
})
export class MainLayoutComponent implements OnInit {

  constructor(private router: Router) {
    this.routeEvent(this.router);
  }

  ngOnInit() {
  }

  routeEvent(router: Router) {
    router.events.subscribe((url: any) => {
      const navigation = document.querySelector(".navigation");
      if (navigation) {
        if (router.url === "/") {
          navigation.classList.add("bg-transparent");
        } else {
          navigation.classList.remove("bg-transparent");
        }
      }
    });
  }

}
