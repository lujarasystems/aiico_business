import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import { ProductsService } from 'src/app/services/products.service';
import { UtilService } from 'src/app/services/util.service';
import { UtilityService } from 'src/app/services/utility.service';
import { InsuranceVm } from 'src/app/models/insurance';


import { EXTRA_INFO, YEARS_OF_DRIVING } from 'src/app/shared/data';
import { MotorProductService } from 'src/app/services/motor-product.service';
import { PersonalAccidentService } from 'src/app/services/personal-accident.service';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';

@Component({
  selector: "app-shop",
  templateUrl: "./shop.component.html",
  styleUrls: ["./shop.component.scss"]
})
export class ShopComponent implements OnInit {
  today = new Date();
  current$: BehaviorSubject<number>;
  buttonText$: BehaviorSubject<string>;
  date = null; // new Date();
  selectedValue$: BehaviorSubject<string>;
  isVisible = false;
  personalDetailsDisabled: BehaviorSubject<boolean>;
  productDetailsDisabled: BehaviorSubject<boolean>;
  uploadDetailsDisabled: BehaviorSubject<boolean>;
  selectedOption: any;
  options$: BehaviorSubject<Array<object>>;
  validIds = [
    { label: "International Passport", value: "international-passport" },
    { label: "National ID", value: "national-id" },
    { label: "Voter's card", value: "voters-card" },
  ];
  formatterNaira = (value: number) => `NGN ${value}`;
  parserNaira = (value: string) => value.replace("NGN ", "");

  planCategory$: BehaviorSubject<string>;
  subProductId$: BehaviorSubject<string>;
  selectedPlan$: BehaviorSubject<any>;
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  extraInfo$: BehaviorSubject<Object>;
  titles = [];
  genders = [];
  insurance: InsuranceVm;

  isStepOneValid$ = new BehaviorSubject(false);
  isStepTwoValid$ = new BehaviorSubject(false);
  isStepThreeValid$ = new BehaviorSubject(false);

  formErrors$ = new BehaviorSubject([]);
  cat: any;
  countries: any;
  clientCats: any;
  travelPlanObj: { subClassCoverTypes: { productName: string; subClassName: string; }; };
  errorArray: any[];
  caption: string;
  yearsOfDriving: any;

  fileData: File = null;

  attachedFilesNames = {
    id: '',
    license: '',
    bill: '',
    passport: ''
  };

  constructor(private accidentService: PersonalAccidentService, public utilService: UtilService, private route: ActivatedRoute, private router: Router, public productService: ProductsService, public utilityService: UtilityService, public motorService: MotorProductService) {
    this.current$ = new BehaviorSubject(0);
    this.buttonText$ = new BehaviorSubject("");
    this.selectedValue$ = new BehaviorSubject("");
    this.options$ = new BehaviorSubject(this.validIds);
    this.personalDetailsDisabled = new BehaviorSubject(true);
    this.productDetailsDisabled = new BehaviorSubject(true);
    this.uploadDetailsDisabled = new BehaviorSubject(true);

    this.planCategory$ = new BehaviorSubject("");
    this.subProductId$ = new BehaviorSubject("");

    this.selectedPlan$ = new BehaviorSubject({});
    this.extraInfo$ = new BehaviorSubject(new Object());

    this.insurance = new InsuranceVm();
  }

  setCaption() {
    if (this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.productName == "Private Motor Comprehensive") {
      this.caption = "Vehicle"
    }
    if (this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.productName == "Private Motor Third Party") {
      this.caption = "Vehicle"
    }
    if (this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.productName == "Personal Accident") {
      this.caption = "Health"
    }
    if (this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.productName == "Home Content Insurance") {
      this.caption = "Home"
    }
    if (this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.productName == "Office Content Insurance") {
      this.caption = "Office"
    }
    if (this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.productName == "Shop Content Insurance") {
      this.caption = "Shop"
    }
    if (this.selectedPlan$.value == 'Travel') {
      this.caption = "Travel"
    }
  }

  ngOnInit() {
    this.yearsOfDriving = YEARS_OF_DRIVING;
    this.insurance = JSON.parse(localStorage.getItem('insurance'));
    if (!this.insurance) {
      this.insurance = new InsuranceVm();
    }
    else {
      this.insurance.vehicleAmount = this.formatAmountToString(this.insurance.vehicleAmount);
    }

    this.route.paramMap.subscribe(params => {
      this.subProductId$.next(params.get("subProductId"));
      this.planCategory$.next(params.get("category"));
    });

    // if history exists

    if (this.planCategory$.value !== 'travel') {
      if (this.utilityService.isObjectEmpty(window.history.state)) {
        this.selectedPlan$.next(window.history.state.data);
        this.setCaption();
        localStorage.setItem('selectedPlan', JSON.stringify(this.selectedPlan$.value));
      } else {
        // get specific product from api
        this.loading$.next(true);
        this.error$.next(false);
        this.productService.getSingleProductSubClass(this.subProductId$.value).subscribe(
          data => {
            if (data) {
              this.selectedPlan$.next(data.result);
              this.setCaption();
              localStorage.setItem('selectedPlan', JSON.stringify(data.result));
            }
            this.loading$.next(false);
          },
          error => {
            this.errorStatus$.next(`${error.status}`);
            this.errorMessage$.next(error.statusText);
            // this.error$.next(true);
            this.loading$.next(false);
          });
      }
    }

    else {
      this.selectedPlan$ = new BehaviorSubject("");
      this.selectedPlan$.next('Travel');
      this.travelPlanObj = {
        subClassCoverTypes: {
          productName: 'Travel',
          subClassName: 'Travel'
        }
      }

      localStorage.setItem('selectedPlan', JSON.stringify(this.travelPlanObj));
      localStorage.setItem("parentProductCat", JSON.stringify('travel'));

      this.setCaption();
      this.loading$.next(true);
      this.error$.next(false);

      this.utilService.GetCountries().subscribe(c => {
        this.countries = c.result;
        this.loading$.next(false);
        localStorage.setItem('countries', JSON.stringify(this.countries));
      }), error => {
        this.errorStatus$.next(`${error.status}`);
        this.errorMessage$.next(error.statusText);
        this.error$.next(true);
        this.loading$.next(false);
      }
    }

    const localData = localStorage.getItem('allProducts');

    if (localData) {
      const products = JSON.parse(localData);
      const object = products.filter(product => product.productCategory.toLowerCase() === this.planCategory$.value)[0];
      const info = EXTRA_INFO.filter(info => info.id === object.id)[0];
      this.extraInfo$.next(info);
    }

    this.getButtonText(this.current$.value);
    this.getTitles();
    this.getGenders();
  }

  formatAmountToString(val) {
    try {

      if (val) {
        val = this.formatAmountToNumber(val);
        return (parseInt(val.toString().replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US');
      }
    }
    catch (e) { }
  }

  formatAmountToNumber(val) {
    if (val)
      return val = typeof val == 'number' ? val : parseFloat(val.replace(/,/g, ''));
  }

  getTitles() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetTitles().subscribe(data => {
      if (data) {

        this.titles = [...data.result];
        localStorage.setItem('titles', JSON.stringify(this.titles));
      }
      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  getGenders() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetGenders().subscribe(data => {
      if (data) {

        this.genders = [...data.result];
        localStorage.setItem('genders', JSON.stringify(this.genders));
      }
      this.loading$.next(false);

    }, error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    });
  }

  setTitle(event) {
    var title = this.titles.find(g => g.name == event);
    this.insurance.titleId = title.id;
  }

  setGender(event) {
    var gender = this.genders.find(g => g.name == event);
    this.insurance.genderId = gender.id;
  }

  setCountry(event) {
    var country = this.countries.find(c => c.name == event);
    this.insurance.countryId = country.id;
  }

  increaseCurrentNumber() {
    localStorage.setItem("insurance", JSON.stringify(this.insurance));
    localStorage.setItem("attachedFilesNames", JSON.stringify(this.attachedFilesNames))

    if (this.current$.value < 2) {
      // check if step is valid

      if (this.checkSteps(this.current$.value)) {
        this.current$.next(this.current$.value + 1);
        return this.getButtonText(this.current$.value);
      }

    } else {
      localStorage.subProductId = this.subProductId$.value;
      if (this.planCategory$.value == 'casualty') {
        this.postPersonalAccidentSchedule();
      }
      else {
        this.router.navigateByUrl(`payment/${this.planCategory$.value}`);
      }
    }
  }

  postPersonalAccidentSchedule() {
    var unit = this.insurance.unit;
    var gender = this.insurance.gender;
    var isFullYear = this.insurance.isFullYear;

    this.insurance.yearsOfDrvExperience = null
    this.insurance.sumAssured = 0;
    this.insurance.productId = this.selectedPlan$.value.subClassCoverTypes.productId;
    this.insurance.subclassSectCovtypeId = this.selectedPlan$.value.subClassCoverTypes.id;
    this.insurance.premiumAmount = parseFloat(this.insurance.premiumAmount.toString());
    this.insurance.unit = parseInt(this.insurance.unit.toString());
    this.insurance.isFullYear = !!this.insurance.isFullYear;
    this.insurance.gender = null;
    this.loading$.next(true);
    this.error$.next(false);
    this.accidentService.PostPersonalAccidentSchedule(this.insurance).subscribe(res => {
      if (res) {
        this.insurance.premiumAmount = res.result.premiumAmount;
        this.insurance.unit = unit;
        this.insurance.gender = gender;
        this.insurance.isFullYear = isFullYear;

        localStorage.setItem("insurance", JSON.stringify(this.insurance));
        this.router.navigate([`payment/${this.planCategory$.value}`], { state: res.result });

      }
      this.loading$.next(false);
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  decreaseCurrentNumber() {
    if (this.current$.value > 0) {
      this.formErrors$.next([]);
      this.current$.next(this.current$.value - 1);
      return this.getButtonText(this.current$.value);
    }
  }

  isInputValid(name) {
    if (this.formErrors$.value.length) {
      return this.formErrors$.value[0].toLowerCase().includes(name);
    }
    return false;
  };

  checkSteps(currentStep): boolean {
    if (currentStep === 0) {
      return this.validateStepOne();
    }

    if (currentStep === 1) {
      return this.validateStepTwo();
    }

    if (currentStep === 2) {
      return this.validateUploads();
    }
  }

  validateStepOne() {
    this.formErrors$.next([]);
    const errorArray = [];

    if (!this.insurance.title) {
      errorArray.push('Please fill in your title');
    } else if (!this.insurance.firstName) {
      errorArray.push('Please fill in your first name');

    } else if (!this.insurance.lastName) {
      errorArray.push('Please fill in your surname');

    } else if (!this.insurance.pryEmail) {
      errorArray.push('Please fill in your email');

    } else if (!this.utilityService.validateEmail(this.insurance.pryEmail)) {
      errorArray.push('Email address is invalid');

    } else if (!this.insurance.gender) {
      errorArray.push('Please select gender');

    } else if (!this.insurance.smsTel) {
      errorArray.push('Please fill in your phone number');

    } else if (!this.insurance.dateOfBirth) {
      errorArray.push('Please fill in your date of birth');

    } else if (!this.insurance.address && this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.subClassName !== 'Personal Accident') {
      errorArray.push('Please fill in your address');
    }
    else if (!this.insurance.physicalAddrs && this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.subClassName == 'Personal Accident') {
      errorArray.push('Please fill in your address');
    }
    else if (!this.insurance.natureOfBusiness && this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.productName == 'Shop Content Insurance') {
      errorArray.push('Please fill in your nature of business');
    }

    else if (!this.insurance.passportNumber && this.planCategory$.value == 'travel') {
      errorArray.push('Please fill in your passport number');
    }

    else if (!this.insurance.dlIssueDate && this.planCategory$.value === 'auto') {
      errorArray.push('Please fill in your license expiry date');
    } else if (!this.insurance.yearsOfDrvExperience && this.planCategory$.value === 'auto') {
      errorArray.push('Please fill in your years of experience');

    } else if (!this.insurance.dlNo && this.planCategory$.value === 'auto') {
      errorArray.push('Please fill in your license number');
    }

    this.formErrors$.next(errorArray);
    return !errorArray.length;
  }

  validateStepTwo() {
    this.formErrors$.next([]);

    this.errorArray = [];

    if (this.planCategory$.value === 'auto') {
      this.insurance.vehicleAmount = this.formatAmountToNumber(this.insurance.vehicleAmount);

      if (!this.insurance.vehicleAmount && this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.subClassName == "Private Motor Comprehensive") {
        this.errorArray.push('Please fill in the vehicle amount');

      }
      if (this.insurance.vehicleAmount < 1000000) {
        this.errorArray.push('Minimum vehicle amount allowed is 1 million naira');

      } else if (!this.insurance.bodyType) {
        this.errorArray.push('Please fill in the body type');

      } else if (!this.insurance.regNo) {
        this.errorArray.push('Please fill in the plate');

      } else if (!this.insurance.engineNo) {
        this.errorArray.push('Please fill in the engine number');

      } else if (!this.insurance.chasisNo) {
        this.errorArray.push('Please fill in the chasis number');

      } else if (!this.insurance.wefDt) {
        this.errorArray.push('Please fill in the start date');

      } else if (!this.insurance.yrManft) {
        this.errorArray.push('Please fill in the year');

      } else if (!this.insurance.make) {
        this.errorArray.push('Please fill in the vehicle make');

      } else if (!this.insurance.model) {
        this.errorArray.push('Please fill in the vehicle model');

      } else if (!this.insurance.color) {
        this.errorArray.push('Please fill in the vehicle color');
      }
    }

    if (this.planCategory$.value === 'casualty') {
      if (!this.insurance.occupation) {
        this.errorArray.push('Please fill in your occupation');
      }

      if (!this.insurance.accidentRate) {
        this.errorArray.push('Please fill in your accident rate');
      }

      if (!this.insurance.unit) {
        this.errorArray.push('Please fill in the unit');
      }

      if (!this.insurance.premiumAmount) {
        this.errorArray.push('Please fill in the premium amount');
      }

      if (!this.insurance.dlIssueDate) {
        this.errorArray.push('Please fill in the inception date');
      }
    }

    if (this.selectedPlan$.value !== 'Travel') {

      if (this.selectedPlan$.value.subClassCoverTypes.subClassName === 'Home Insurance' || this.selectedPlan$.value.subClassCoverTypes.subClassName === 'Office Insurance') {
        if (!this.insurance.preOwnership) {
          this.errorArray.push('Please fill in your ownership detail');
        }

        if (!this.insurance.lgaId) {
          this.errorArray.push('Please fill in your house location');
        }

        if (!this.insurance.tenancy) {
          this.errorArray.push('Please fill in the Tenancy Type');
        }

        if (!this.insurance.coverDuration) {
          this.errorArray.push('Please fill in the cover duration');
        }

        if (!this.insurance.wef) {
          this.errorArray.push('Please fill in the start date');
        }

        if (!this.insurance.postalAddress) {
          this.errorArray.push('Please fill in your postal address');
        }

        if (!this.insurance.description) {
          this.errorArray.push('Please fill in the description');
        }

        if (!this.insurance.physicalAddrs) {
          this.errorArray.push('Please fill in the address');
        }
      }
    }

    if (this.selectedPlan$.value !== 'Travel') {
      if (this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.subClassName === 'Shop Content Insurance') {
        if (!this.insurance.typeOfShop) {
          this.errorArray.push('Please fill in the type of shop');
        }

        if (!this.insurance.natureOfStructure) {
          this.errorArray.push('Please fill in the nature of structure');
        }

        if (!this.insurance.natureOfStock) {
          this.errorArray.push('Please fill in the nature of stock');
        }

        if (!this.insurance.stockTakingMode) {
          this.errorArray.push('Please fill in stock taking mode');
        }

        if (!this.insurance.keepsStockBook) {
          this.errorArray.push('Please fill in your book store prefernce');
        }

        if (!this.insurance.keepsStockBookInSafePlace) {
          this.errorArray.push('Please fill your choice of keeping books');
        }

        if (!this.insurance.lgaId) {
          this.errorArray.push('Please fill in the lga of shop');
        }

        if (!this.insurance.stockAmount) {
          this.errorArray.push('Please fill in maximum stock value');
        }

        if (!this.insurance.coverDuration) {
          this.errorArray.push('Please fill in cover duration');
        }

        if (!this.insurance.wef) {
          this.errorArray.push('Please fill in start date');
        }

        if (!this.insurance.address) {
          this.errorArray.push('Please fill in the address');
        }
      }
    }

    if (this.planCategory$.value === 'travel') {
      if (!this.insurance.travelPurpose) {
        this.errorArray.push('Please fill in your travel purpose');
      }

      if (!this.insurance.preMedical) {
        this.errorArray.push('Please fill in Pre-existing medical condition');
      }

      if (!this.insurance.nokName) {
        this.errorArray.push('Please fill in next of kin name');
      }

      if (!this.insurance.noKrelationship) {
        this.errorArray.push('Please fill in relationship with next of kin');
      }

      if (!this.insurance.noKaddr) {
        this.errorArray.push('Please fill in address of next of kin');
      }
    }


    this.formErrors$.next(this.errorArray);
    return !this.errorArray.length;
  }

  validateUploads() {
    if (this.planCategory$.value === 'auto') {
      return !!(this.insurance.IdentificationUrl && this.insurance.VehicleLicenseUrl && this.insurance.UtilityBillUrl);
    }

    if (this.planCategory$.value === 'travel') {
      return !!(this.insurance.PassportUrl);
    }

    if (this.planCategory$.value === 'content' || this.planCategory$.value === 'casualty' || this.selectedPlan$.value.subClassCoverTypes.productName ===
      'Shop Content Insurance') {
      return !!(this.insurance.UtilityBillUrl);
    }

    return true;
  }

  goto(step) {
    if (step < 3) {
      if (this.current$.value < step) {
        if (this.checkSteps(this.current$.value)) {
          this.current$.next(step);
          return this.getButtonText(this.current$.value);
        }
      }
      else {
        this.current$.next(step);
        return this.getButtonText(this.current$.value);
      }

    }

  }

  getButtonText(currentValue) {
    switch (currentValue) {
      case 2:
        return this.buttonText$.next("SAVE AND PROCEED TO PAYMENT");
      case 3:
        return this.buttonText$.next("CONFIRM AND SUBMIT");
      case 4:
        return this.buttonText$.next("COMPLETE PAYMENT");

      default:
        return this.buttonText$.next("NEXT");
    }
  }

  handleChange(file: any, selectedValue: string): void {

    this.fileData = <File>file.target.files[0];

    var reader: FileReader = new FileReader();

    reader.readAsDataURL(this.fileData);
    reader.onloadend = (e) => {

      switch (selectedValue) {
        case "id":
          this.insurance.IdentificationUrl = reader.result as string;
          this.attachedFilesNames.id = this.fileData.name;
          break;

        case "license":
          this.insurance.VehicleLicenseUrl = reader.result as string;
          this.attachedFilesNames.license = this.fileData.name;
          break;

        case "bill":
          this.insurance.UtilityBillUrl = reader.result as string;
          this.attachedFilesNames.bill = this.fileData.name;
          break;

        default:
          this.insurance.PassportUrl = reader.result as string;
          this.attachedFilesNames.passport = this.fileData.name;
          break;
      }

      this.validateUploads();

    }
  }

  makeEditable(value) {
    return value.next(!value.value);
  }

  onChange(result: Date): void {
    // console.log("onChange: ", result);
  }

  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    return differenceInCalendarDays(current, this.today) < 0;
  };

  disabledDOB = (current: Date): boolean => {
    if (this.planCategory$.value == 'content' || this.planCategory$.value == 'auto') {
      var date = new Date();
      var eighteenYearsAgo = date.getFullYear() - 18;
      date.setFullYear(eighteenYearsAgo)
      return differenceInCalendarDays(current, date) > 0;
    }
  };
}
