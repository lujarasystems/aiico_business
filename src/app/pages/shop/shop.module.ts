import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ShopComponent } from "./shop.component";
import { AutoComponent } from 'src/app/components/auto/auto.component';
import { HealthComponent } from 'src/app/components/health/health.component';
import { HomeComponent } from 'src/app/components/home/home.component';
import { TravelComponent } from 'src/app/components/travel/travel.component';

export const routes: Routes = [{ path: "", component: ShopComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [ShopComponent, AutoComponent, TravelComponent, HealthComponent, HomeComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ShopModule { }
