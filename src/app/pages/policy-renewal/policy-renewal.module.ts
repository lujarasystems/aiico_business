import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PolicyRenewalComponent } from './policy-renewal.component';
import { SharedModule } from '../../shared/shared.module';
import { RavepaymentModule } from 'src/app/components/ravepay/ravepay.module';

export const routes: Routes = [{ path: "", component: PolicyRenewalComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes), RavepaymentModule],
  declarations: [PolicyRenewalComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PolicyRenewalModule { }
