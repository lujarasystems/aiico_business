import { Component, OnInit, NgZone, Inject, Renderer2 } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { InsuranceVm } from 'src/app/models/insurance';
import { NzMessageService } from 'ng-zorro-antd';
import { UtilityService } from 'src/app/services/utility.service';
import { RaveOptionsModel } from 'src/app/models/raveOptions';
import { InterswitchOptionsModel } from 'src/app/models/interswitchOptions';
import { RenewalService } from 'src/app/services/renewal.service';
import { sha512 } from 'js-sha512';
import { ProductsService } from 'src/app/services/products.service';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-policy-renewal',
  templateUrl: './policy-renewal.component.html',
  styleUrls: ['./policy-renewal.component.scss']
})

export class PolicyRenewalComponent implements OnInit {
  current$: BehaviorSubject<number>;
  productDetailsDisabled: BehaviorSubject<boolean>;
  selectedPlan: any;
  formErrors$ = new BehaviorSubject([]);

  showFlutterwavePayment: boolean = false;
  paymentOption = '';
  raveOptions: RaveOptionsModel;

  transactionRef: string;
  interswitchOptions: InterswitchOptionsModel;
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  extraInfo$: BehaviorSubject<Object>;

  constructor(private msg: NzMessageService, public utilityService: UtilityService, public renewalService: RenewalService, private productService: ProductsService, private router: Router, private ngZone: NgZone, @Inject(DOCUMENT) private document: Document, private renderer: Renderer2) {
    this.current$ = new BehaviorSubject(0);
    this.productDetailsDisabled = new BehaviorSubject(true);
    this.interswitchOptions = new InterswitchOptionsModel();
    this.raveOptions = new RaveOptionsModel();

    if (!this.renewalService.renewalDetails) {
      window.history.back();
    }
  }


  ngOnInit() {
    this.formatAmountToString(this.renewalService.renewalDetails.nextInstallmentPremium);
  }

  isInputValid(name) {
    if (this.formErrors$.value.length) {
      return this.formErrors$.value[0].toLowerCase().includes(name);
    }
    return false;
  };

  generatePaymentOptions(data) {
    // rave
    this.raveOptions.txref = data.transactionRef;
    this.raveOptions.customer_email = data.email ? data.email : 'segsybro@gmail.com';
    const customerName = data.customerName.split(" ");
    this.raveOptions.customer_firstname = customerName[2] ? customerName[2] : customerName[1];
    this.raveOptions.customer_lastname = customerName[0];
    this.raveOptions.amount = data.amount;
    this.raveOptions.customer_phone = data.phone;
    this.raveOptions.custom_description = "Life insurance policy renewals";

    // interswitch
    this.interswitchOptions.amount = data.amount * 100;
    this.interswitchOptions.txn_ref = data.transactionRef;
    this.interswitchOptions.name = data.customerName;
    this.interswitchOptions.pay_item_name = "Life insurance policy renewals";
    const token = this.interswitchOptions.txn_ref + this.interswitchOptions.product_id + this.interswitchOptions.pay_item_id + this.interswitchOptions.amount + this.interswitchOptions.redirect_url + this.interswitchOptions.macKey;
    this.interswitchOptions.hash = sha512(token).toUpperCase();
  }

  confirmFormFields() {

    if (this.checkSteps(this.current$.value)) {
      this.renewalService.renewalDetails.nextInstallmentPremium = this.formatAmountToNumber(this.renewalService.renewalDetails.nextInstallmentPremium);
      var schedule = {
        policyNo: this.renewalService.renewalDetails.policyNumber,
        transactionDate: this.renewalService.renewalDetails.paidToDate,
        customerName: this.renewalService.renewalDetails.clientName,
        email: this.renewalService.renewalDetails.email,
        phone: this.renewalService.renewalDetails.phone,
        amount: this.renewalService.renewalDetails.nextInstallmentPremium
      }

      this.loading$.next(true);
      this.error$.next(false);
      this.renewalService.PostLifeRenewalSchedule(schedule).subscribe(res => {
        res
        this.transactionRef = res.result.transactionRef;
        this.generatePaymentOptions(res.result);
        this.loading$.next(false);
        this.current$.next(this.current$.value + 1);

      }), error => {
        this.errorStatus$.next(`${error.status}`);
        this.errorMessage$.next(error.statusText);
        // this.error$.next(true);
        this.loading$.next(false);
      }

    }

  }

  checkSteps(currentStep): boolean {
    if (currentStep === 0) {
      return this.validate();
    }
  }

  validate() {
    this.formErrors$.next([]);
    const errorArray = [];

    if (!this.renewalService.renewalDetails.email) {
      errorArray.push('Please fill in your email address');
    } else if (!this.renewalService.renewalDetails.phone) {
      errorArray.push('Please fill in your phone number');

    } else if (!this.renewalService.renewalDetails.nextInstallmentPremium) {
      errorArray.push('Please fill in your next installment premium');

    }
    this.formErrors$.next(errorArray);
    return !errorArray.length;
  }

  goto(step) {
    return this.current$.next(step);
  }

  confirmPayment(response: any): void {
    console.log(response);

    const { tx } = response;

    this.saveTransactionRef(tx);

    this.cancelledPayment();

    this.ngZone.run(() => {
      const elem = this.document.querySelector('[name=checkout]');
      this.renderer.removeChild(this.document.body, elem);
      this.renderer.addClass(this.document.body, 'overflow-auto');
      this.router.navigateByUrl("result/flutterwave");
    });
  }

  cancelledPayment(): void {
    console.log("transaction completed");
  }

  formatAmountToString(val) {
    try {

      if (val) {
        this.renewalService.renewalDetails
          .nextInstallmentPremium = this.formatAmountToNumber(val);
        this.renewalService.renewalDetails
          .nextInstallmentPremium = (parseInt(val.toString().replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US');
      }
    }
    catch (e) { }
  }

  formatAmountToNumber(val) {
    if (val)
      return val = typeof val == 'number' ? val : parseFloat(val.replace(/,/g, ''));
  }

  saveTransactionRef(tx) {
    const transactionDetails = {
      product: this.raveOptions.custom_description,
      txnref: this.transactionRef,
      ispolicyrenewal: true,
      chargeResponseCode: tx.chargeResponseCode,
      chargeResponseMessage: tx.chargeResponseMessage
    }

    localStorage.setItem('transactionDetails', JSON.stringify(transactionDetails));
  }
}
