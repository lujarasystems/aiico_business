import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RegisterComponent } from './register.component';

export const routes: Routes = [{ path: "", component: RegisterComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [RegisterComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class RegisterModule { }
