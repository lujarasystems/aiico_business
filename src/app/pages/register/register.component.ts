import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { InsuranceVm } from 'src/app/models/insurance';
import { PersonalAccidentService } from 'src/app/services/personal-accident.service';
import { UtilService } from 'src/app/services/util.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { ProductsService } from 'src/app/services/products.service';
import { UtilityService } from 'src/app/services/utility.service';
import { MotorProductService } from 'src/app/services/motor-product.service';
import { YEARS_OF_DRIVING, EXTRA_INFO } from 'src/app/shared/data';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  today = new Date();
  current$: BehaviorSubject<number>;
  buttonText$: BehaviorSubject<string>;
  date = null; // new Date();
  selectedValue$: BehaviorSubject<string>;
  isVisible = false;
  personalDetailsDisabled: BehaviorSubject<boolean>;
  productDetailsDisabled: BehaviorSubject<boolean>;
  uploadDetailsDisabled: BehaviorSubject<boolean>;
  selectedOption: any;
  options$: BehaviorSubject<Array<object>>;
  validIds = [
    { label: "International Passport", value: "international-passport" },
    { label: "National ID", value: "national-id" },
    { label: "Voter's card", value: "voters-card" },
  ];
  formatterNaira = (value: number) => `NGN ${value}`;
  parserNaira = (value: string) => value.replace("NGN ", "");

  planCategory$: BehaviorSubject<string>;
  subProductId$: BehaviorSubject<string>;
  selectedPlan$: BehaviorSubject<any>;
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  extraInfo$: BehaviorSubject<Object>;
  titles = [];
  genders = [];
  insurance: InsuranceVm;

  isStepOneValid$ = new BehaviorSubject(false);
  isStepTwoValid$ = new BehaviorSubject(false);
  isStepThreeValid$ = new BehaviorSubject(false);

  formErrors$ = new BehaviorSubject([]);
  cat: any;
  countries: any;
  clientCats: any;
  travelPlanObj: { subClassCoverTypes: { productName: string; subClassName: string; }; };
  errorArray: any[];
  selectedDl: any;
  selectedVl: any;
  selectedUb: any;
  selectedIp: any;
  caption: string;
  yearsOfDriving: any;
  loadingVehicleDetails: boolean;
  vehicleMakeList: any[];
  vehicleModelList: any[];
  bodyTypes: any[];
  currencySymbol: any;
  dateRange = [];
  destinationWeather: any;
  currentLocationWeather: number;
  @ViewChild('amount', null) vehicleAmount: ElementRef;

  constructor(public utilService: UtilService, private route: ActivatedRoute, private msg: NzMessageService, private router: Router, public productService: ProductsService, public utilityService: UtilityService, public motorService: MotorProductService) {
    this.current$ = new BehaviorSubject(0);
    this.buttonText$ = new BehaviorSubject("");
    this.selectedValue$ = new BehaviorSubject("");
    this.options$ = new BehaviorSubject(this.validIds);
    this.personalDetailsDisabled = new BehaviorSubject(true);
    this.productDetailsDisabled = new BehaviorSubject(true);
    this.uploadDetailsDisabled = new BehaviorSubject(true);

    this.planCategory$ = new BehaviorSubject("");
    this.subProductId$ = new BehaviorSubject("");

    this.selectedPlan$ = new BehaviorSubject({});
    this.extraInfo$ = new BehaviorSubject(new Object());

    this.insurance = new InsuranceVm();
  }

  ngOnInit() {
    this.yearsOfDriving = YEARS_OF_DRIVING;
    this.insurance = JSON.parse(localStorage.getItem('insurance'));
    if (!this.insurance) {
      this.insurance = new InsuranceVm();
    }
    else {
      this.formatAmountToString(this.insurance.vehicleAmount);
    }

    this.route.paramMap.subscribe(params => {
      this.subProductId$.next(params.get("subProductId"));
      this.planCategory$.next(params.get("category"));
    });

    // if history exists

    if (this.planCategory$.value !== 'travel') {

      if (this.utilityService.isObjectEmpty(window.history.state)) {
        this.selectedPlan$.next(window.history.state.data);
        localStorage.setItem('selectedPlan', JSON.stringify(this.selectedPlan$.value));
      } else {
        // get specific product from api
        this.loading$.next(true);
        this.error$.next(false);
        this.productService.getSingleProductSubClass(this.subProductId$.value).subscribe(
          data => {
            if (data) {
              this.selectedPlan$.next(data.result);
              localStorage.setItem('selectedPlan', JSON.stringify(data.result));
            }
            this.loading$.next(false);
          },
          error => {
            this.errorStatus$.next(`${error.status}`);
            this.errorMessage$.next(error.statusText);
            // this.error$.next(true);
            this.loading$.next(false);
          });
      }
      this.getClientCats();
    }

    else {
      this.selectedPlan$ = new BehaviorSubject("");
      this.selectedPlan$.next('Travel');
      this.travelPlanObj = {
        subClassCoverTypes: {
          productName: 'Travel',
          subClassName: 'Travel'
        }
      }
    }

    const localData = localStorage.getItem('allProducts');
    if (localData) {
      const products = JSON.parse(localData);
      const object = products.filter(product => product.productCategory.toLowerCase() === this.planCategory$.value)[0];
      const info = EXTRA_INFO.filter(info => info.id === object.id)[0];
      this.extraInfo$.next(info);
    }

    this.getTitles();
  }

  getClientCats() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetClientCategories().subscribe(c => {
      if (c) {
        this.clientCats = c.result;
        localStorage.setItem('clientCats', JSON.stringify(this.clientCats));
        this.insurance.clientCategory = this.clientCats.find(c => c.name == "Individual").name;
        this.insurance.clientCategoryId = this.clientCats.find(c => c.name == "Individual").id;
      }
      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  getTitles() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetTitles().subscribe(data => {
      if (data) {

        this.titles = [...data.result];
        localStorage.setItem('titles', JSON.stringify(this.titles));
      }
      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  setTitle(event) {
    var title = this.titles.find(g => g.name == event);
    this.insurance.titleId = title.id;
  }

  setGender(event) {
    var gender = this.genders.find(g => g.name == event);
    this.insurance.genderId = gender.id;
  }

  getCountries() {
    this.loading$.next(true);
    this.utilService.GetCountries().subscribe(c => {
      this.countries = c.result;
      this.loading$.next(false);
      localStorage.setItem('countries', JSON.stringify(this.countries));
      this.current$.next(this.current$.value + 1);
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  setCountry(event) {
    var country = this.countries.find(c => c.name == event);
    this.insurance.countryId = country.id;
  }

  increaseCurrentNumber() {
    localStorage.setItem("insurance", JSON.stringify(this.insurance));

    if (this.current$.value < 2) {
      // check if step is valid

      if (this.checkSteps(this.current$.value) && this.planCategory$.value == 'auto') {
        if (this.current$.value == 0) {
          this.GetBodyTypes()
        }
        if (this.current$.value == 1) {
          if (this.selectedPlan$.value.subClassCoverTypes.productName == 'Private Motor Comprehensive') {
            this.computeComprehensivePremium();
          }

          else if (this.selectedPlan$.value.subClassCoverTypes.productName == 'Private Motor Third Party') {
            this.computeThirdPartyPremium();
          }

        }
      }

      else if (this.checkSteps(this.current$.value) && this.planCategory$.value == 'travel') {
        if (this.current$.value == 0) {
          this.getCountries();
        }
        if (this.current$.value == 1) {
          this.computeTravelPremium();

        }
      }

    } else {
      localStorage.subProductId = this.subProductId$.value;

      if (this.planCategory$.value == 'auto') {
        this.router.navigate(['/shop', this.planCategory$.value, this.selectedPlan$.value.subClassCoverTypes.id], { state: this.selectedPlan$.value })
      }
      else if (this.planCategory$.value == 'travel') {
        this.router.navigate(['/shop', this.planCategory$.value, this.subProductId$.value]);

      }
    }
  }

  setClientCategory(event) {

    var client = this.clientCats.find(c => c.name == event);
    this.insurance.clientId = client.id;
    this.insurance.clientCategoryId = client.id;
  }

  decreaseCurrentNumber() {
    if (this.current$.value > 0) {
      this.formErrors$.next([]);
      this.current$.next(this.current$.value - 1);
    }
  }

  getWeatherForecast() {
    try {
      this.utilService.getCountryCapital(this.insurance.country).subscribe(res => {
        if (res) {
          var date = this.insurance.wef as any;
          date = date.toString().substring(0, date.toString().length - 1) as string;

          date = date.split(" - ").map(function (date) {
            return Math.floor(Date.parse(date + "-0500") / 1000);
          }).join(" - ");

          var lat = res[0].latlng[0];
          var long = res[0].latlng[1];
          this.getWeatherForCity(lat, long, date);

        }
        else {
          this.loading$.next(false);
          this.current$.next(this.current$.value + 1);
        }
      }), error => {
        this.loading$.next(false);
      }
    }
    catch (e) {
      this.loading$.next(false);
    }

  }

  getWeatherForCity(lat, long, date) {
    this.utilService.GetWeatherForCity(lat, long, date).subscribe(res => {
      if (res && res.daily) {
        this.destinationWeather = (parseFloat(res.daily.data[0].apparentTemperatureLow) + parseFloat(res.daily.data[0].apparentTemperatureHigh)) / 2;
        this.destinationWeather = Math.floor((this.destinationWeather - 32) * 5 / 9)
        this.getWeatherForNigeria(10, 8, date);
      }
      else {
        this.loading$.next(false);
        this.current$.next(this.current$.value + 1);
      }

    }), error => {
      this.loading$.next(false);
    }
  }

  getWeatherForNigeria(lat, long, date) {
    this.utilService.GetWeatherForCity(lat, long, date).subscribe(res => {
      if (res && res.daily) {
        if (res.timezone.includes("Lagos")) {
          this.currentLocationWeather = (parseFloat(res.daily.data[0].apparentTemperatureLow) + parseFloat(res.daily.data[0].apparentTemperatureHigh)) / 2;
          this.currentLocationWeather = Math.floor((this.currentLocationWeather - 32) * 5 / 9)
          this.loading$.next(false);
          this.current$.next(this.current$.value + 1);
        }
      }
      else {
        this.current$.next(this.current$.value + 1);
        this.loading$.next(false);
      }

    }), error => {
      this.loading$.next(false);
    }
  }

  isInputValid(name) {
    if (this.formErrors$.value.length) {
      return this.formErrors$.value[0].toLowerCase().includes(name);
    }
    return false;
  };

  checkSteps(currentStep): boolean {
    if (currentStep === 0) {
      return this.validateStepOne();
    }

    if (currentStep === 1) {
      return this.validateStepTwo();
    }
  }

  validateStepOne() {
    this.formErrors$.next([]);
    const errorArray = [];

    if (!this.insurance.title) {
      errorArray.push('Please fill in your title');
    } else if (!this.insurance.firstName) {
      errorArray.push('Please fill in your first name');

    } else if (!this.insurance.lastName) {
      errorArray.push('Please fill in your surname');
    }

    else if (!this.insurance.clientCategory && this.planCategory$.value == 'auto') {
      errorArray.push('Please fill in your Category');
    }

    this.formErrors$.next(errorArray);
    return !errorArray.length;
  }

  validateStepTwo() {
    this.formErrors$.next([]);
    this.insurance.vehicleAmount = this.formatAmountToNumber(this.insurance.vehicleAmount);
    this.errorArray = [];

    if (this.planCategory$.value === 'auto') {
      if (!this.insurance.vehicleAmount && this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.subClassName == "Private Motor Comprehensive") {
        this.errorArray.push('Please fill in the vehicle amount');

      }
      if (this.insurance.vehicleAmount < 1000000 && this.selectedPlan$.value.subClassCoverTypes && this.selectedPlan$.value.subClassCoverTypes.subClassName == "Private Motor Comprehensive") {
        this.errorArray.push('Minimum vehicle amount allowed is 1 million naira');

      } else if (!this.insurance.bodyType) {
        this.errorArray.push('Please fill in the body type');

      } else if (!this.insurance.regNo) {
        this.errorArray.push('Please fill in the plate');
      }
    }

    else if (this.planCategory$.value == 'travel') {
      if (!this.insurance.wef) {
        this.errorArray.push('Please fill in travel date from');
      }
      else if (!this.insurance.wet) {
        this.errorArray.push('Please fill in travel date to');
      }
      else if (!this.insurance.country) {
        this.errorArray.push('Please fill in the destination');
      }
      else if (!this.insurance.dateOfBirth) {
        this.errorArray.push('Please fill in your date of birth');
      }
    }

    this.formatAmountToString(this.insurance.vehicleAmount);
    this.formErrors$.next(this.errorArray);
    return !this.errorArray.length;
  }

  getVehicleDetails() {
    this.loadingVehicleDetails = true;
    this.error$.next(false);

    this.motorService.GetVehicleDetails(this.insurance.regNo).subscribe(res => {
      if (res) {
        this.insurance.chasisNo = res.result.licenseInfo.chasisNo;
        this.insurance.color = res.result.licenseInfo.color;
        this.insurance.engineNo = res.result.licenseInfo.engineNo;
        this.insurance.make = res.result.licenseInfo.vehicleMakeName;
        this.insurance.yrManft = res.result.licenseInfo.year;
        this.insurance.wefDt = res.result.licenseInfo.isssueDate
        this.insurance.regNo = res.result.licenseInfo.registrationNo;
        this.insurance.model = res.result.licenseInfo.model;
        this.loadingVehicleDetails = false;
        localStorage.setItem("insurance", JSON.stringify(this.insurance));
        this.GetMotorMake(this.insurance.yrManft);
      }

      else {
        this.loadingVehicleDetails = false;
        this.loading$.next(false);
        this.current$.next(this.current$.value + 1);
      }

    }), error => {
      this.loadingVehicleDetails = false;
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  computeComprehensivePremium() {
    if (this.selectedPlan$.value.subClassCoverTypes.productName == 'Private Motor Comprehensive') {
      this.insurance.vehicleAmount = this.formatAmountToNumber(this.insurance.vehicleAmount);
      this.insurance.premiumAmount = this.insurance.vehicleAmount * (this.selectedPlan$.value.subClassCoverTypes.rate / 100);
      this.formatAmountToString(this.insurance.vehicleAmount);
      this.loading$.next(true);
      this.getVehicleDetails();

    }
  }

  computeThirdPartyPremium() {
    if (this.selectedPlan$.value.subClassCoverTypes.productName == 'Private Motor Third Party') {
      this.loading$.next(true);
      this.utilService.ComputeThirdPartyPremium(this.insurance.bodyType).subscribe(res => {
        this.insurance.premiumAmount = res.result;
        localStorage.setItem('insurance', JSON.stringify(this.insurance));
        this.getVehicleDetails();
      }), error => {
        this.loading$.next(false);
      };
    }

  }

  GetMotorModel(make, year) {
    this.error$.next(false);

    this.utilService.GetVehicleMakeModel(make, year).subscribe(data => {
      if (data) {
        this.vehicleModelList = [...data.result];
        localStorage.setItem("vehicleModelList", JSON.stringify(this.vehicleModelList));
        localStorage.setItem("insurance", JSON.stringify(this.insurance));

        this.current$.next(this.current$.value + 1);
        this.loading$.next(false);
      }
      else {
        this.loading$.next(false);
      }

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  GetMotorMake(year) {
    this.error$.next(false);
    // this.insurance.make = null;
    this.utilService.GetVehicleMake(year).subscribe(data => {
      if (data) {
        this.vehicleMakeList = [...data.result];
        localStorage.setItem("vehicleMakeList", JSON.stringify(this.vehicleMakeList));
        localStorage.setItem("insurance", JSON.stringify(this.insurance));
        this.GetMotorModel(this.insurance.make, this.insurance.yrManft);
      }

      else {
        this.loading$.next(false);
      }

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  GetBodyTypes() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetBodyTypes().subscribe(data => {
      if (data) {
        this.bodyTypes = [...data.result];
        localStorage.setItem("bodyTypes", JSON.stringify(this.bodyTypes));

        this.current$.next(this.current$.value + 1);
      }

      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  computeTravelPremium() {
    var obj = {
      dateOfBirth: typeof this.insurance.dateOfBirth == "string" ? this.insurance.dateOfBirth : this.insurance.dateOfBirth.toDateString(),
      destinationCountryId: this.insurance.countryId,
      startDate: typeof this.insurance.wef == "string" ? this.insurance.wef : this.insurance.wef.toDateString(),
      endDate: typeof this.insurance.wet == "string" ? this.insurance.wet : this.insurance.wet.toDateString(),
    }
    this.loading$.next(true);
    this.error$.next(false);
    this.productService.ComputeTravelPremium(obj).subscribe(res => {
      if (res) {
        this.insurance.sumAssured = res.result.sumAssured;
        this.insurance.excess = res.result.excess;
        this.insurance.premium = res.result.premium;
        this.insurance.currencySymbol = res.result.currencySymbol;
        localStorage.setItem("insurance", JSON.stringify(this.insurance));
        this.getWeatherForecast();
      }
      else {
        this.loading$.next(false);
      }

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  formatAmountToString(val) {
    try {

      if (val) {
        this.insurance.vehicleAmount = this.formatAmountToNumber(val);
        this.insurance.vehicleAmount = (parseInt(val.toString().replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US');
      }
    }
    catch (e) { }
  }

  formatAmountToNumber(val) {
    if (val)
      return val = typeof val == 'number' ? val : parseFloat(val.replace(/,/g, ''));
  }

  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    return differenceInCalendarDays(current, this.today) < 0;
  };

  disabledDOB = (current: Date): boolean => {
    if (this.planCategory$.value == 'content' || this.planCategory$.value == 'auto') {
      var date = new Date();
      var eighteenYearsAgo = date.getFullYear() - 18;
      date.setFullYear(eighteenYearsAgo)
      return differenceInCalendarDays(current, date) > 0;
    }
  };
}
