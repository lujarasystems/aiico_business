import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home.component";
import { SlickCarouselModule } from "ngx-slick-carousel";
import { ClientsComponent } from "src/app/components/clients/clients.component";


export const routes: Routes = [{ path: "", component: HomeComponent }];


@NgModule({
  imports: [SharedModule, SlickCarouselModule, RouterModule.forChild(routes)],
  declarations: [HomeComponent, ClientsComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HomeModule { }
