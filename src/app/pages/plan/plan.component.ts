import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import { ProductsService } from 'src/app/services/products.service';
import { Product } from 'src/app/models/product';
import { UtilityService } from 'src/app/services/utility.service';
import { EXTRA_INFO } from '../../shared/data';

@Component({
  selector: "app-plan",
  templateUrl: "./plan.component.html",
  styleUrls: ["./plan.component.scss"]
})
export class PlanComponent implements OnInit {
  plan$: BehaviorSubject<string>;
  selectedProduct$: BehaviorSubject<Product>;
  products$: BehaviorSubject<Array<any>>;
  benefits$: BehaviorSubject<Array<any>>;
  loading$: BehaviorSubject<boolean>;
  extraInfo$: BehaviorSubject<Object>;
  error$: BehaviorSubject<boolean>;
  errorStatus$: BehaviorSubject<string>;
  errorMessage$: BehaviorSubject<string>;

  constructor(private route: ActivatedRoute, private router: Router, public productService: ProductsService, public utilityService: UtilityService) {
    this.plan$ = new BehaviorSubject("");
    this.products$ = new BehaviorSubject([]);
    this.benefits$ = new BehaviorSubject([]);
    this.selectedProduct$ = new BehaviorSubject(new Product());
    this.loading$ = new BehaviorSubject(false);
    this.extraInfo$ = new BehaviorSubject(new Object());
    this.errorStatus$ = new BehaviorSubject('');
    this.errorMessage$ = new BehaviorSubject('Sorry, there is an error on server.');
    this.error$ = new BehaviorSubject(false);
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.plan$.next(params.get("category"));

      this.getProductSubClassTypes(params.get("productId"));

      const info = EXTRA_INFO.filter(info => info.id === params.get("productId"))[0];
      this.extraInfo$.next(info);

      this.productService.allProducts$.subscribe(products => {
        const result = products.filter(product => product.id === params.get("productId"))[0];
        if (result) {
          localStorage.setItem("parentProductCat", JSON.stringify(result.productCategory.toLowerCase()))
          this.selectedProduct$.next(result);
        }
      });

    });
  }

  getObjectKeys(obj: Object) {
    if(obj)
    return Object.keys(obj);
  }

  getBenefits(data) {
    const result = [];
    data.map(item => {
      item.benefits.map(benefit => {
        if (!result.includes(benefit.name)) {
          result.push(benefit.name)
        }
      });
    });

    return result;
  }

  getProductSubClassTypes(productId) {
    this.loading$.next(true);
    this.productService.getProductSubClass(productId).subscribe(
      res => {
        if (res) {
          res.result = res.result.filter(p => p.subClassCoverTypes.coverTypeName !== "Autogenius")
          this.products$.next(res.result);
          this.benefits$.next(this.getBenefits(res.result));
        }
        this.loading$.next(false);
      }, error => {
        this.errorStatus$.next(`${error.status}`);
        this.errorMessage$.next(error.statusText);
        this.error$.next(true);
        this.loading$.next(false);
      });
  }

  getBenefitFromArr(arr: Array<any>, value: string) {
    const result = arr.filter(benefit => benefit.name === value);

    if (result.length !== 0) {
      return result[0];
    }

    return null;
  }


  scrollToDiv(el: HTMLElement) {
    el.scrollIntoView();
  }
}
