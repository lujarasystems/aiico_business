import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlanComponent } from "./plan.component";

export const routes: Routes = [{ path: "", component: PlanComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [PlanComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PlanModule { }
