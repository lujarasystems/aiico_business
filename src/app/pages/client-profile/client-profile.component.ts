import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { EXTRA_INFO } from 'src/app/shared/data';
import { RenewalService } from 'src/app/services/renewal.service';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.scss']
})
export class ClientProfileComponent implements OnInit {
  extraInfo$ = new BehaviorSubject(new Object());
  profile = {};
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  isVisible: boolean;
  selectedTabIndex = 0;

  constructor(public renewalService: RenewalService) {
  }

  ngOnInit() {
    const info = EXTRA_INFO.filter(info => info.summary == "client-agent profile")[0];
    this.extraInfo$.next(info);
    if (!this.renewalService.profile)
      window.history.back();
  }

  onTabChange(i) {
    this.selectedTabIndex = i;
  }

  GetLifePolicyRenewalDetails() {
    // this.loading$.next(true);
    //   this.error$.next(false);

    // this.renewalService.GetLifePolicyRenewalDetails("NCSP/IB/2017/077067").subscribe(res => {
    //   this.profile = res.result;
    //   this.loading$.next(false);
    // }), error => {
    //     this.errorStatus$.next(`${error.status}`);
    //       this.errorMessage$.next(error.statusText);
    //        this.error$.next(true);
    //       this.loading$.next(false);
    // }
  }

  postClientProfiling() {
    var profile = {
      policyNumber: this.renewalService.profile[this.selectedTabIndex].policyNumber,
      policyStatus: this.renewalService.profile[this.selectedTabIndex].policyStatus,
      email: this.renewalService.profile[this.selectedTabIndex].email,
      SmsTelephone: this.renewalService.profile[this.selectedTabIndex].phone,
      Othernames: this.renewalService.profile[this.selectedTabIndex].clientName,
      productDescription: this.renewalService.profile[this.selectedTabIndex].productDescription,
      clientName: this.renewalService.profile[this.selectedTabIndex].clientName,
      Surname: this.renewalService.profile[this.selectedTabIndex].clientName
    };

    this.loading$.next(true);
    this.error$.next(false);

    this.renewalService.PostClientProfiling(profile).subscribe(res => {
      if(res) {
      this.isVisible = true;
      }
      this.loading$.next(false);
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  handleCancel() {
    this.isVisible = false;
  }
}
