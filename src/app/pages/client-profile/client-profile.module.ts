import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ClientProfileComponent } from './client-profile.component';

export const routes: Routes = [{ path: "", component: ClientProfileComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [ClientProfileComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ClientProfileModule { }
