import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AgentProfileComponent } from './agent-profile.component';

export const routes: Routes = [{ path: "", component: AgentProfileComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [AgentProfileComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AgentProfileModule { }
