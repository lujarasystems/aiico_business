import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { RenewalService } from 'src/app/services/renewal.service';
import { EXTRA_INFO } from 'src/app/shared/data';

@Component({
  selector: 'app-agent-profile',
  templateUrl: './agent-profile.component.html',
  styleUrls: ['./agent-profile.component.scss']
})
export class AgentProfileComponent implements OnInit {

  extraInfo$ = new BehaviorSubject(new Object());
  profile = {};
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  isVisible: boolean;

  constructor(public renewalService: RenewalService) {
  }

  ngOnInit() {
    const info = EXTRA_INFO.filter(info => info.summary == "client-agent profile")[0];
    this.extraInfo$.next(info);
    if (!this.renewalService.profile)
    window.history.back();
  }

  GetLifePolicyRenewalDetails() {
    // this.loading$.next(true);
    //   this.error$.next(false);

    // this.renewalService.GetLifePolicyRenewalDetails("NCSP/IB/2017/077067").subscribe(res => {
    //   this.profile = res.result;
    //   this.loading$.next(false);
    // }), error => {
    //     this.errorStatus$.next(`${error.status}`);
    //       this.errorMessage$.next(error.statusText);
    //        this.error$.next(true);
    //       this.loading$.next(false);
    // }
  }

  postAgentProfiling() {
    var profile = {
      agentcode: this.renewalService.profile.agentcode,
      email: this.renewalService.profile.email,
      telephone: this.renewalService.profile.telephone,
      name: this.renewalService.profile.agN_NAME
    };

    this.loading$.next(true);
    this.error$.next(false);

    this.renewalService.PostAgentProfiling(profile).subscribe(res => {
      if(res) {
      this.isVisible = true;
      }
      this.loading$.next(false);
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }
  handleCancel() {
    this.isVisible = false;
  }
}
