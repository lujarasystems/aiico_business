import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PaymentComponent } from "./payment.component";
import { RavepaymentModule } from 'src/app/components/ravepay/ravepay.module';

export const routes: Routes = [{ path: "", component: PaymentComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes), RavepaymentModule],
  declarations: [PaymentComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PaymentModule { }
