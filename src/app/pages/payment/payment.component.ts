import { Component, OnInit, NgZone } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { InsuranceVm } from 'src/app/models/insurance';
import { NzMessageService } from 'ng-zorro-antd';
import { UtilityService } from 'src/app/services/utility.service';
import { RaveOptionsModel } from 'src/app/models/raveOptions';
import { InterswitchOptionsModel } from 'src/app/models/interswitchOptions';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';
import { sha512 } from 'js-sha512';
import { YEARS_OF_DRIVING } from 'src/app/shared/data';
import { ProductsService } from 'src/app/services/products.service';
import { PersonalAccidentService } from 'src/app/services/personal-accident.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService } from 'src/app/services/util.service';
import { MotorProductService } from 'src/app/services/motor-product.service';

@Component({
  selector: "app-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.scss"]
})
export class PaymentComponent implements OnInit {
  plan$: BehaviorSubject<object>;
  current$: BehaviorSubject<number>;
  buttonText$: BehaviorSubject<string>;
  personalDetailsDisabled: BehaviorSubject<boolean>;
  productDetailsDisabled: BehaviorSubject<boolean>;
  uploadDetailsDisabled: BehaviorSubject<boolean>;
  selectedPlan: any;
  titles = [];
  genders = [];
  premiumAmount = 0;
  selectedValue$: BehaviorSubject<string>;
  validIds = [
    { label: "International Passport", value: "international-passport" },
    { label: "National ID", value: "national-id" },
    { label: "Voter's card", value: "voters-card" },
  ];

  formatterNaira = (value: number) => `NGN ${value}`;
  parserNaira = (value: string) => value.replace("NGN ", "");
  insurance: InsuranceVm;
  bodyTypes = [];
  yearsOfManu = [];
  colorList = [];
  vehicleModelList = [];
  vehicleMakeList = [];
  parentProductCat: any;
  localGovtAreas: any;
  accidentRates: any;
  countries: any;
  clientCat: any;
  plan: string;
  isVisible: boolean;
  options$: BehaviorSubject<{ label: string; value: string; }[]>;
  caption: string;

  showFlutterwavePayment: boolean = false;
  paymentOption = '';
  raveOptions: RaveOptionsModel;
  transactionRef: string;
  interswitchOptions: InterswitchOptionsModel;
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);

  confirmDetails: boolean = false;
  today = new Date();
  yearsOfDriving: string[];

  planCategory$: BehaviorSubject<string>;
  subProductId$: BehaviorSubject<string>;
  extraInfo$: BehaviorSubject<Object>;

  fileData: File = null;

  attachedFilesNames = {
    id: '',
    license: '',
    bill: '',
    passport: ''
  };

  constructor(private accidentService: PersonalAccidentService, private route: ActivatedRoute, public utilService: UtilService, private router: Router, public utilityService: UtilityService, public motorService: MotorProductService, private msg: NzMessageService, private productService: ProductsService, private ngZone: NgZone) {
    this.plan$ = new BehaviorSubject({});
    this.current$ = new BehaviorSubject(3);
    this.buttonText$ = new BehaviorSubject("");
    this.productDetailsDisabled = new BehaviorSubject(true);
    this.personalDetailsDisabled = new BehaviorSubject(true);
    this.selectedValue$ = new BehaviorSubject("");
    this.uploadDetailsDisabled = new BehaviorSubject(true);
    this.options$ = new BehaviorSubject(this.validIds);
    this.interswitchOptions = new InterswitchOptionsModel();
    this.raveOptions = new RaveOptionsModel();
    this.planCategory$ = new BehaviorSubject("");
    this.subProductId$ = new BehaviorSubject("");
    this.insurance = new InsuranceVm();
  }

  setPaymentData(data) {
    const paymentData = data;
    this.transactionRef = paymentData.transactionRef;

    // rave
    this.raveOptions.PBFPubKey = 'FLWPUBK-c674c68d40a0cb428926869498f14171-X';
    this.raveOptions.txref = paymentData.transactionRef;
    this.raveOptions.customer_email = paymentData.pryEmail ? paymentData.pryEmail : 'segsybro@gmail.com';
    this.raveOptions.customer_firstname = paymentData.firstName;
    this.raveOptions.customer_lastname = paymentData.lastName;
    this.raveOptions.amount = this.planCategory$.value == 'travel' ? paymentData.premium : paymentData.premiumAmount;
    this.raveOptions.customer_phone = paymentData.smsTel;

    // interswitch
    this.interswitchOptions.amount = this.planCategory$.value == 'travel' ? paymentData.premium * 100 : paymentData.premiumAmount * 100;
    this.interswitchOptions.txn_ref = paymentData.transactionRef;
    this.interswitchOptions.name = `${paymentData.firstName}  ${paymentData.lastName}`;
    const token = this.interswitchOptions.txn_ref + this.interswitchOptions.product_id + this.interswitchOptions.pay_item_id + this.interswitchOptions.amount + this.interswitchOptions.redirect_url + this.interswitchOptions.macKey;
    this.interswitchOptions.hash = sha512(token).toUpperCase();
  }

  ngOnInit() {

    if (localStorage.getItem('insurance')) {
      this.insurance = JSON.parse(localStorage.getItem('insurance'));
      if (this.insurance.vehicleAmount)
        this.insurance.vehicleAmount = this.formatAmountToString(this.insurance.vehicleAmount);
      if (this.insurance.premiumAmount)
        this.insurance.premiumAmount = this.formatAmountToNumber(this.insurance.premiumAmount);
    }

    if (localStorage.getItem('attachedFilesNames')) {
      this.attachedFilesNames = JSON.parse(localStorage.getItem('attachedFilesNames'));
    }

    this.route.paramMap.subscribe(params => {
      this.planCategory$.next(params.get("category"));
    });

    this.subProductId$.next(localStorage.getItem('subProductId'));
    this.selectedPlan = (window.history.state.data);
    this.selectedPlan = JSON.parse(localStorage.getItem('selectedPlan'));

    try {
      if (this.planCategory$.value == 'casualty') {
        if (window.history.state.premiumAmount) {
          this.setPaymentData(window.history.state);
        }
        else {
          window.history.back();
        }
      }

      if (this.planCategory$.value == 'travel' && this.insurance && !this.insurance.premium) {
        var allProducts = JSON.parse(localStorage.getItem('allProducts')) as any;
        var pid = allProducts.find(p => p.productCategory == "Travel").id;
        this.router.navigate([`/register/${this.planCategory$.value}/${pid}`]);
      }

      this.raveOptions.custom_description = this.selectedPlan.subClassCoverTypes.productName;

      this.interswitchOptions.pay_item_name = this.selectedPlan.subClassCoverTypes.productName;

      if (this.selectedPlan && this.selectedPlan.subClassCoverTypes.subClassName == 'Home Insurance') {
        this.plan = "House";
      }
      else if (this.selectedPlan && this.selectedPlan.subClassCoverTypes.subClassName == 'Office Insurance') {
        this.plan = "Office"
      }
      this.setCaption();
      this.parentProductCat = JSON.parse(localStorage.getItem('parentProductCat'));
      // this.insurance = JSON.parse(localStorage.getItem('insurance'));
      this.yearsOfDriving = YEARS_OF_DRIVING;
      this.loadFormData();
    }
    catch (e) {
      if (this.planCategory$.value == 'travel') {
        var allProducts = JSON.parse(localStorage.getItem('allProducts')) as any;
        var pid = allProducts.find(p => p.productCategory == "Travel").id;
        this.router.navigate([`/register/${this.planCategory$.value}/${pid}`]);
      }
      else if (this.planCategory$.value == 'auto') {
        this.router.navigate([`/register/${this.planCategory$.value}/${this.selectedPlan.subClassCoverTypes.id}`]);
      }
      else {
        window.history.back();
      }
    }
  }

  formatAmountToString(val) {
    try {
      if (val) {
        val = this.formatAmountToNumber(val);
        return val = (parseInt(val.toString().replace(/[^\d]+/gi, '')) || 0).toLocaleString('en-US');
      }
    }
    catch (e) { }
  }

  formatAmountToNumber(val) {
    if (val)
      return val = typeof val == 'number' ? val : parseFloat(val.replace(/,/g, ''));
  }

  setCaption() {
    if (this.selectedPlan.subClassCoverTypes.productName == "Private Motor Comprehensive") {
      this.caption = "Vehicle"
    }
    if (this.selectedPlan.subClassCoverTypes.productName == "Private Motor Third Party") {
      this.caption = "Vehicle"
    }
    if (this.selectedPlan.subClassCoverTypes.productName == "Personal Accident") {
      this.caption = "Health"
    }
    if (this.selectedPlan.subClassCoverTypes.productName == "Home Content Insurance") {
      this.caption = "Home"
    }
    if (this.selectedPlan.subClassCoverTypes.productName == "Office Content Insurance") {
      this.caption = "Office"
    }
    if (this.selectedPlan.subClassCoverTypes.productName == "Shop Content Insurance") {
      this.caption = "Shop"
    }
    if (this.selectedPlan.subClassCoverTypes.productName == "Travel") {
      this.caption = "Travel"
    }
  }

  loadFormData() {
    this.titles = JSON.parse(localStorage.getItem('titles'));
    this.genders = JSON.parse(localStorage.getItem('genders'));
    this.bodyTypes = JSON.parse(localStorage.getItem('bodyTypes'));
    this.yearsOfManu = JSON.parse(localStorage.getItem('yearsOfManu'));
    this.colorList = JSON.parse(localStorage.getItem('colorList'));
    this.vehicleModelList = JSON.parse(localStorage.getItem('vehicleModelList'));
    this.vehicleMakeList = JSON.parse(localStorage.getItem('vehicleMakeList'));
    this.localGovtAreas = JSON.parse(localStorage.getItem('lgas'));
    this.accidentRates = JSON.parse(localStorage.getItem('accidentRates'));
    this.countries = JSON.parse(localStorage.getItem('countries'));
    this.clientCat = JSON.parse(localStorage.getItem('clientCats'))
  }

  confirmFormFields() {
    if (this.planCategory$.value == 'auto') {
      this.PostMotorSchedule();
    }
    else if (this.planCategory$.value == 'casualty') {
      this.PostPersonalAccidentSchedule();
    }
    else if (this.planCategory$.value == 'travel') {
      this.PostTravelSchedule()
    }
    else if (this.selectedPlan.subClassCoverTypes.productName == "Home Content Insurance") {
      this.PostHomeSchedule();
    }
    else if (this.selectedPlan.subClassCoverTypes.productName == "Office Content Insurance") {
      this.PostOfficeSchedule();
    }
    else if (this.selectedPlan.subClassCoverTypes.productName == "Shop Content Insurance") {
      this.PostShopSchedule();
    }
  }

  goto(step) {
    return this.current$.next(step);
  }

  mergeContent(option) {
    return (option.name + " - " + option.value);
  }

  makeEditable(value) {

    // rave
    this.raveOptions.customer_email = this.insurance.pryEmail ? this.insurance.pryEmail : 'segsybro@gmail.com';
    this.raveOptions.customer_firstname = this.insurance.firstName;
    this.raveOptions.customer_lastname = this.insurance.lastName;
    this.raveOptions.amount = this.insurance.premiumAmount;
    this.raveOptions.customer_phone = this.insurance.smsTel;

    // interswitch
    this.interswitchOptions.amount = this.insurance.premiumAmount;
    return value.next(!value.value);
  }

  showModal(options): void {
    this.isVisible = true;
    this.selectedValue$.next(options);
  }

  disabledDate = (current: Date): boolean => {
    // Can not select days before today and today
    var x = differenceInCalendarDays(current, this.today) < 0;
    return x;
  };

  disabledDOB = (current: Date): boolean => {
    if (this.planCategory$.value == 'content' || this.planCategory$.value == 'auto') {
      var date = new Date();
      var eighteenYearsAgo = date.getFullYear() - 18;
      date.setFullYear(eighteenYearsAgo)
      return differenceInCalendarDays(current, date) > 0;
    }
  };

  onChangeVehicleAmount(val) {
    this.insurance.vehicleAmount = val;
    this.insurance.vehicleAmount = this.formatAmountToNumber(this.insurance.vehicleAmount);
    if (this.insurance.vehicleAmount > 1000000) {
      this.insurance.premiumAmount = (this.selectedPlan.subClassCoverTypes.rate / 100) * this.insurance.vehicleAmount;
    }
    else {
      this.insurance.vehicleAmount = 1000000;
      this.insurance.premiumAmount = (this.selectedPlan.subClassCoverTypes.rate / 100) * this.insurance.vehicleAmount;
      this.insurance.vehicleAmount = this.formatAmountToString(this.insurance.vehicleAmount);
    }
  }

  onChangeStockValue() {
    this.insurance.stockAmount = this.formatAmountToString(this.insurance.stockAmount);
  }

  validateUploads() {
    if (this.parentProductCat === 'auto') {
      return !!(this.insurance.IdentificationUrl && this.insurance.VehicleLicenseUrl && this.insurance.UtilityBillUrl);
    }

    if (this.parentProductCat === 'content' || this.parentProductCat === 'casualty' || this.selectedPlan.subClassCoverTypes.productName ===
      'Shop Content Insurance') {
      return !!(this.insurance.UtilityBillUrl);
    }

    if (this.parentProductCat === 'travel') {
      return !!(this.insurance.PassportUrl);
    }

    return true;
  }

  handleChange(file: any, selectedValue: string): void {

    this.fileData = <File>file.target.files[0];

    var reader: FileReader = new FileReader();

    reader.readAsDataURL(this.fileData);
    reader.onloadend = (e) => {

      switch (selectedValue) {
        case "id":
          this.insurance.IdentificationUrl = reader.result as string;
          this.attachedFilesNames.id = this.fileData.name;
          break;

        case "license":
          this.insurance.VehicleLicenseUrl = reader.result as string;
          this.attachedFilesNames.license = this.fileData.name;
          break;

        case "bill":
          this.insurance.UtilityBillUrl = reader.result as string;
          this.attachedFilesNames.bill = this.fileData.name;
          break;

        default:
          this.insurance.PassportUrl = reader.result as string;
          this.attachedFilesNames.passport = this.fileData.name;
          break;
      }

      this.validateUploads();

    }
  }

  PostTravelSchedule() {
    this.loading$.next(true);
    this.insurance.isActive = false;
    this.insurance.medical = null;
    this.insurance.preMedical = !!this.insurance.preMedical;
    this.insurance.productId = this.subProductId$.value;
    this.insurance.transDate = new Date(Date.now()).toDateString();
    localStorage.setItem("insurance", JSON.stringify(this.insurance));
    this.productService.PostTravelSchedule(this.insurance).subscribe(res => {
      if (res) {
        this.setPaymentData(res.result);
        this.current$.next(this.current$.value + 1);
        this.clearStorage();
      }
      this.loading$.next(false);
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  PostPersonalAccidentSchedule() {
    this.insurance.yearsOfDrvExperience = null
    this.insurance.sumAssured = 0;
    this.insurance.productId = this.selectedPlan.subClassCoverTypes.productId;
    this.insurance.subclassSectCovtypeId = this.selectedPlan.subClassCoverTypes.id;
    this.insurance.premiumAmount = parseFloat(this.insurance.premiumAmount.toString());
    this.insurance.unit = parseInt(this.insurance.unit.toString());
    this.insurance.isFullYear = !!this.insurance.isFullYear;
    this.insurance.gender = null;
    this.loading$.next(true);
    this.error$.next(false);
    this.accidentService.PostPersonalAccidentSchedule(this.insurance).subscribe(res => {
      if (res) {
        this.setPaymentData(res.result);
        this.current$.next(this.current$.value + 1);
        this.clearStorage();
      }
      this.loading$.next(false);
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  PostMotorSchedule() {
    this.insurance.cubicCap = null;
    this.insurance.yearsOfDrvExperience = parseInt(this.insurance.yearsOfDrvExperience.substring(0, 2));
    this.insurance.productId = this.selectedPlan.subClassCoverTypes.productId;
    this.insurance.subclassSectCovtypeId = this.selectedPlan.subClassCoverTypes.id;
    this.loading$.next(true);
    this.error$.next(false);
    localStorage.setItem("insurance", JSON.stringify(this.insurance));

    this.motorService.PostMotorSchedule(this.insurance).subscribe(res => {
      if (res) {
        this.setPaymentData(res.result);
        this.current$.next(this.current$.value + 1);
        this.clearStorage();
      }
      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  PostOfficeSchedule() {
    this.insurance.productId = this.selectedPlan.subClassCoverTypes.productId;
    this.insurance.subclassSectCovtypeId = this.selectedPlan.subClassCoverTypes.id;
    this.insurance.valueOfContents = 0;
    this.insurance.sumAssured = 0;
    this.insurance.yearsOfDrvExperience = null;
    this.insurance.premiumAmount = 10000;
    this.insurance.transactionRef = "";
    this.insurance.dlIssueDate = null;
    this.insurance.gender = null;
    this.insurance.dlNo = null;
    this.insurance.townId = null;
    this.insurance.dateOfBirth = typeof this.insurance.dateOfBirth == "string" ? this.insurance.dateOfBirth : this.insurance.dateOfBirth.toDateString() as any;
    this.insurance.intermediaryId = null;
    this.insurance.wef = typeof this.insurance.wef == "string" ? this.insurance.wef : this.insurance.wef.toDateString() as any;
    this.insurance.wet = null;
    if (this.insurance.coverDuration == 'N10,000/annum') {
      this.insurance.isFullYear = true;
    }
    else {
      this.insurance.isFullYear = false;
    }

    this.insurance.officeItems.forEach(i => {
      i.amount = this.formatAmountToNumber(i.amount);
      // i.amount = parseFloat(i.amount.toString());
      this.insurance.valueOfContents += parseFloat(i.amount.toString());
    });
    this.insurance.valueOfContents = this.insurance.valueOfContents.toString();

    this.loading$.next(true);
    this.error$.next(false);
    this.productService.PostOfficeSchedule(this.insurance).subscribe(res => {
      this.loading$.next(false);
      if (res) {
        this.setPaymentData(res.result);
        this.current$.next(this.current$.value + 1);
        this.clearStorage();
      }
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  PostHomeSchedule() {
    this.insurance.productId = this.selectedPlan.subClassCoverTypes.productId;
    this.insurance.subclassSectCovtypeId = this.selectedPlan.subClassCoverTypes.id;
    this.insurance.valueOfContents = 0;
    this.insurance.sumAssured = 0;
    this.insurance.premiumAmount = 10000;
    this.insurance.transactionRef = "";
    this.insurance.dlIssueDate = null;
    this.insurance.gender = null;
    this.insurance.yearsOfDrvExperience = null;

    this.insurance.dlNo = null;
    this.insurance.townId = null;
    this.insurance.dateOfBirth = typeof this.insurance.dateOfBirth == "string" ? this.insurance.dateOfBirth : this.insurance.dateOfBirth.toDateString() as any;
    this.insurance.intermediaryId = null;
    this.insurance.wef = typeof this.insurance.wef == "string" ? this.insurance.wef : this.insurance.wef.toDateString() as any;
    this.insurance.wet = null;
    if (this.insurance.coverDuration == 'N10,000/annum') {
      this.insurance.isFullYear = true;
    }
    else {
      this.insurance.isFullYear = false;
    }

    this.insurance.homeItems.forEach(i => {
      i.amount = this.formatAmountToNumber(i.amount);
      // i.amount = parseFloat(i.amount.toString());
      this.insurance.valueOfContents += parseFloat(i.amount.toString());
    });
    this.insurance.valueOfContents = this.insurance.valueOfContents.toString();

    this.loading$.next(true);
    this.error$.next(false);
    this.productService.PostHomeSchedule(this.insurance).subscribe(res => {
      this.loading$.next(false);
      if (res) {
        this.setPaymentData(res.result);
        this.current$.next(this.current$.value + 1);
        this.clearStorage();
      }
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  PostShopSchedule() {
    this.insurance.productId = this.selectedPlan.subClassCoverTypes.productId;
    this.insurance.subclassSectCovtypeId = this.selectedPlan.subClassCoverTypes.id;
    this.insurance.valueOfContents = 0;
    this.insurance.sumAssured = 0;
    this.insurance.premiumAmount = 10000;
    this.insurance.transactionRef = "";
    this.insurance.dlIssueDate = null;
    this.insurance.gender = null;
    this.insurance.dlNo = null;
    this.insurance.townId = null;
    this.insurance.keepsStockBook = !!this.insurance.keepsStockBook;
    this.insurance.keepsStockBookInSafePlace = !!this.insurance.keepsStockBookInSafePlace;
    this.insurance.dateOfBirth = typeof this.insurance.dateOfBirth == "string" ? this.insurance.dateOfBirth : this.insurance.dateOfBirth.toDateString() as any;
    this.insurance.intermediaryId = null;
    this.insurance.wef = typeof this.insurance.wef == "string" ? this.insurance.wef : this.insurance.wef.toDateString() as any;
    this.insurance.wet = null;
    if (this.insurance.coverDuration == 'N10,000/annum') {
      this.insurance.isFullYear = true;
    }
    else {
      this.insurance.isFullYear = false;
    }

    this.loading$.next(true);
    this.error$.next(false);
    this.productService.PostShopSchedule(this.insurance).subscribe(res => {
      this.loading$.next(false);
      if (res) {
        this.setPaymentData(res.result);
        this.current$.next(this.current$.value + 1);
        this.clearStorage();
      }
    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    }
  }

  confirmPayment(response: any): void {

    const { tx } = response;

    this.saveTransactionRef(tx);

    this.cancelledPayment();

    this.ngZone.run(() => this.router.navigateByUrl("result/flutterwave"));

  }

  cancelledPayment(): void {
    console.log("transaction completed");
  }

  saveTransactionRef(tx) {
    const transactionDetails = {
      product: this.raveOptions.custom_description,
      txnref: this.transactionRef,
      ispolicyrenewal: false,
      chargeResponseCode: tx.chargeResponseCode,
      chargeResponseMessage: tx.chargeResponseMessage
    }

    localStorage.setItem('transactionDetails', JSON.stringify(transactionDetails));
  }

  clearStorage() {
    localStorage.removeItem('attachedFilesNames');
    localStorage.removeItem('insurance');
    localStorage.removeItem('titles');
    localStorage.removeItem('selectedPlan');
    localStorage.removeItem('countries');
    localStorage.removeItem('subProductId');
    localStorage.removeItem('parentProductCat');
    localStorage.removeItem('clientCats');
    localStorage.removeItem('genders');
    localStorage.removeItem('accidentRates');
    localStorage.removeItem('casualtyPaymentData');
  }
}
