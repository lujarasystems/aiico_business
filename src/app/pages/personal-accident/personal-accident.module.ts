import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PersonalAccidentComponent } from '../personal-accident/personal-accident.component';

export const routes: Routes = [{ path: "", component: PersonalAccidentComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [PersonalAccidentComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class PersonalAccidentModule { }
