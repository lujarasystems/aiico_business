import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Product } from 'src/app/models/product';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { UtilService } from 'src/app/services/util.service';
import differenceInCalendarDays from 'date-fns/differenceInCalendarDays';

@Component({
  selector: 'app-personal-accident',
  templateUrl: './personal-accident.component.html',
  styleUrls: ['./personal-accident.component.scss']
})
export class PersonalAccidentComponent implements OnInit {
  package$: BehaviorSubject<Product>;
  plan$: BehaviorSubject<object>;
  current$: BehaviorSubject<number>;
  buttonText$: BehaviorSubject<string>;
  date = null; // new Date();
  selectedValue$: BehaviorSubject<string>;
  isVisible = false;
  personalDetailsDisabled: BehaviorSubject<boolean>;
  productDetailsDisabled: BehaviorSubject<boolean>;
  uploadDetailsDisabled: BehaviorSubject<boolean>;
  selectedOption: object;
  options$: BehaviorSubject<Array<object>>;
  validIds = [
    { label: "International Passport", value: "international-passport" },
    { label: "National ID", value: "national-id" },
    { label: "Voter's card", value: "voters-card" }
  ];
  formatterNaira = (value: number) => `NGN ${value}`;
  parserNaira = (value: string) => value.replace("NGN ", "");

  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  titles = [];
  today = new Date();

  constructor(private route: ActivatedRoute, private msg: NzMessageService, public utilService: UtilService) {
    this.package$ = new BehaviorSubject(new Product());
    this.plan$ = new BehaviorSubject(new Object());
    this.current$ = new BehaviorSubject(0);
    this.buttonText$ = new BehaviorSubject("");
    this.selectedValue$ = new BehaviorSubject("");
    this.options$ = new BehaviorSubject(this.validIds);
    this.personalDetailsDisabled = new BehaviorSubject(true);
    this.productDetailsDisabled = new BehaviorSubject(true);
    this.uploadDetailsDisabled = new BehaviorSubject(true);
  }

  ngOnInit() {
    this.getButtonText(this.current$.value);
    this.getTitles();
  }

  getTitles() {
    this.loading$.next(true);
    this.error$.next(false);

    this.utilService.GetTitles().subscribe(data => {
      if (data) {
        this.titles = [...data.result];
      }
      this.loading$.next(false);

    }), error => {
      this.errorStatus$.next(`${error.status}`);
      this.errorMessage$.next(error.statusText);
      this.error$.next(true);
      this.loading$.next(false);
    };
  }

  increaseCurrentNumber() {
    if (this.current$.value < 5) {
      this.current$.next(this.current$.value + 1);
      return this.getButtonText(this.current$.value);
    }
  }

  decreaseCurrentNumber() {
    if (this.current$.value > 0) {
      this.current$.next(this.current$.value - 1);
      return this.getButtonText(this.current$.value);
    }
  }

  getButtonText(currentValue) {
    if (currentValue === 2) {
      return this.buttonText$.next("SAVE AND PROCEED TO PAYMENT");
    }
    if (currentValue === 3) {
      return this.buttonText$.next("CONFIRM AND SUBMIT");
    }
    if (currentValue === 4) {
      return this.buttonText$.next("COMPLETE PAYMENT");
    }

    return this.buttonText$.next("NEXT");
  }

  disabledDate = (current: Date): boolean => {
    return differenceInCalendarDays(current, this.today) < 0;
  };

  getPlan(packageData, planId) {
    if (!(!packageData || packageData.length === 0)) {
      const planData = packageData.data.filter(item => item.id === planId);
      if (!(!planData || planData.length === 0)) {
        return this.plan$.next(planData[0]);
      }
    }
  }

  handleChange({ file, fileList }: { [key: string]: any }): void {
    const status = file.status;

    if (status === "done") {
      this.msg.success(`${file.name} file uploaded successfully.`);
    } else if (status === "error") {
      this.msg.error(`${file.name} file upload failed.`);
    }
  }

  makeEditable(value) {
    return value.next(!value.value);
  }

  onChange(result: Date): void {
    // console.log("onChange: ", result);
  }

  showModal(options): void {
    this.isVisible = true;
    this.selectedValue$.next(options);
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  log(value: { label: string; value: string; }): void {
    // console.log(value);
  }

  mergeContent(option) {
    return (option.name + " - " + option.value);
  }

}
