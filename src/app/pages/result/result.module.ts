import { SharedModule } from "../../shared/shared.module";
import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ResultComponent } from './result.component';


export const routes: Routes = [{ path: "", component: ResultComponent }];

@NgModule({
  imports: [SharedModule, RouterModule.forChild(routes)],
  declarations: [ResultComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ResultModule { }
