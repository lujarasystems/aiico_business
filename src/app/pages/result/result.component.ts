import { Component, OnInit } from '@angular/core';
import { PaymentService } from 'src/app/services/payment.service';
import { BehaviorSubject } from "rxjs";
import { ProductsService } from 'src/app/services/products.service';
import { InterswitchOptionsModel } from 'src/app/models/interswitchOptions';
import { TransactionModel } from 'src/app/models/transaction';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  transactionStatus$: BehaviorSubject<string>;
  transactionDescription$: BehaviorSubject<string>;
  interswitchOptions: InterswitchOptionsModel;
  loading$ = new BehaviorSubject(false);
  errorStatus$ = new BehaviorSubject('');
  errorMessage$ = new BehaviorSubject('');
  error$ = new BehaviorSubject(false);
  paymentChannel$ = new BehaviorSubject('');

  transactionData$ = new BehaviorSubject(new TransactionModel());

  constructor(public paymentService: PaymentService, private productService: ProductsService, private route: ActivatedRoute) {
    this.transactionStatus$ = new BehaviorSubject('');
    this.transactionDescription$ = new BehaviorSubject('');
    this.interswitchOptions = new InterswitchOptionsModel();
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const paymentChannel = params.get("paymentChannel");
      this.paymentChannel$.next(paymentChannel);
      const transactionDetails = localStorage.getItem("transactionDetails");

      if (transactionDetails) {
        if (paymentChannel !== 'flutterwave') {
          const { txnref, amount, product, ispolicyrenewal } = JSON.parse(transactionDetails);

          this.loading$.next(true);

          this.paymentService.getTransactionDetails(txnref, amount).subscribe(res => {
            console.log(res);
            this.loading$.next(false);

            this.finalizeInterswitchPayment(txnref, amount, product, ispolicyrenewal, res.success);

            if (res.success) {
              this.transactionStatus$.next("Payment Successful");
              this.transactionDescription$.next(`${product} was paid for successfully`);
            } else {
              this.transactionStatus$.next("Payment Failed");
              this.transactionDescription$.next(res.error);
            }

          }, err => {
            this.transactionStatus$.next("Transaction Failed");
            this.transactionDescription$.next(err.error.message);
            this.loading$.next(false);
          });

        } else {
          const { txnref, product, ispolicyrenewal, chargeResponseCode, chargeResponseMessage } = JSON.parse(transactionDetails);

          this.finalizeRavePayment(txnref, product, ispolicyrenewal, chargeResponseCode)

          if (chargeResponseCode == '00' || chargeResponseCode == '0') {
            this.transactionStatus$.next("Payment Successful");
            this.transactionDescription$.next(`${product} was paid for successfully`);
          } else {
            this.transactionStatus$.next("Payment Failed");
            this.transactionDescription$.next(chargeResponseMessage);
          }
        }
      } else {
        this.transactionStatus$.next("Transaction Failed");
        this.transactionDescription$.next("transaction details was not provided");
        this.loading$.next(false);
      }
    });

  }

  finalizeInterswitchPayment(transactionRef, amount, product, ispolicyrenewal, isSuccessful): any {
    this.loading$.next(true);
    this.error$.next(false);

    var ref = {
      transactionRef,
      amount,
      interswitchMacKey: this.interswitchOptions.macKey
    }

    let transactionData = new TransactionModel();
    transactionData.txnref = transactionRef;
    transactionData.productType = product;

    if (isSuccessful) {
      transactionData.gatewayResponse = "Approved Successful";
    } else {
      transactionData.gatewayResponse = "Transaction Failed";
    }

    if (ispolicyrenewal) {
      this.productService.FinalizeInterswitchPolicyPayment(ref).subscribe(res => {
        this.loading$.next(false);
        transactionData = { ...this.extractInterswitchData(transactionData, res.result) };
        console.log(transactionData);
        this.transactionData$.next(transactionData);

      }), error => {
        this.errorStatus$.next(`${error.status}`);
        this.errorMessage$.next(error.statusText);
        this.error$.next(true);
        this.loading$.next(false);
      }
    } else {
      this.productService.FinalizeInterswitchPayment(ref).subscribe(res => {
        this.loading$.next(false);
        console.log(res);
        transactionData = { ...this.extractInterswitchData(transactionData, res.result) };
        console.log(transactionData);
        this.transactionData$.next(transactionData);
      }), error => {
        this.errorStatus$.next(`${error.status}`);
        this.errorMessage$.next(error.statusText);
        this.error$.next(true);
        this.loading$.next(false);
      }
    }

  }

  extractInterswitchData(transactionData, apiResponse) {
    const { item1, item2 } = apiResponse;

    transactionData.policies = item2.policies;
    transactionData.printPolicyUrl = item2.printPolicyUrl;
    transactionData.totalAmount = item2.totalAmount;
    transactionData.wef = item2.wef;
    transactionData.wet = item2.wet;
    transactionData.cardNumber = item1.cardNumber;
    transactionData.paymentReference = item1.paymentReference;

    return transactionData;
  }


  finalizeRavePayment(transactionRef, product, ispolicyrenewal, chargeResponseCode): any {
    this.loading$.next(true);
    this.error$.next(false);

    let transactionData = new TransactionModel();
    transactionData.txnref = transactionRef;
    transactionData.productType = product;

    if (chargeResponseCode == "00" || chargeResponseCode == "0") {
      transactionData.gatewayResponse = "Approved Successful";
    } else {
      transactionData.gatewayResponse = "Transaction Failed";
    }

    if (ispolicyrenewal) {
      this.productService.FinalizeFlutterwavePolicyPayment(transactionRef).subscribe(res => {
        this.loading$.next(false);
        console.log(res);

        transactionData = { ...this.extractRaveData(transactionData, res.result) };
        this.transactionData$.next(transactionData);
        this.loading$.next(false);

      }), error => {
        this.errorStatus$.next(`${error.status}`);
        this.errorMessage$.next(error.statusText);
        this.error$.next(true);
        this.loading$.next(false);
      }
    } else {
      this.productService.FinalizeFlutterwavePayment(transactionRef).subscribe(res => {
        this.loading$.next(false);
        console.log(res);

        transactionData = { ...this.extractRaveData(transactionData, res.result) };
        this.transactionData$.next(transactionData);
        this.loading$.next(false);

      }), error => {
        this.errorStatus$.next(`${error.status}`);
        this.errorMessage$.next(error.statusText);
        this.error$.next(true);
        this.loading$.next(false);
      }
    }
  }

  extractRaveData(transactionData, apiResponse) {
    const { item1, item2 } = apiResponse;

    transactionData.policies = item2.policies;
    transactionData.printPolicyUrl = item2.printPolicyUrl;
    transactionData.totalAmount = item2.totalAmount;
    transactionData.wef = item2.wef;
    transactionData.wet = item2.wet;
    transactionData.cardNumber = item1.data.card.cardBIN;
    transactionData.paymentReference = item1.data.flw_ref;

    return transactionData;
  }

  joinArr(arr) {
    if (arr) {
      return arr.join(', ');
    }

    return 'N/A';
  }

  formatCurrency(value) {
    const num = parseFloat(value);

    if (num === num) {
      return `\u20A6${value}`;
    }

    return value;
  }
}
