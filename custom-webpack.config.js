module.exports = {
  devServer: {
    setup(app) {
      app.post("*", (req, res) => {
        res.redirect(req.originalUrl);
      });
    }
  }
};
